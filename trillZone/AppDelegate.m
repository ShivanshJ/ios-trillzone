//
//  AppDelegate.m
//  trillZone
//
//  Created by Shivansh on 10/27/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginController.h"
#import <trillSDK/trillSDK.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize adId;
@synthesize device_model;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


//Operations to perform when the application loads
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.progressBarShow = NO;
    //----Device advertisementID
//    self.adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    self.adId = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    self.device_model = [self device_name];
    
    //----SDK Initialize 
    self.tSDK = [[trillSDK alloc] init];
    self.tSDK.delegate = self;
    [self.tSDK initializeSDK:@"4d8c10f9-3b80-48a9-902e-a78dace8963e" :@"com.trillbit.trillzone" completionHandler:^{
        self.DB = [[Database alloc]init];
        [self.DB loadDatabase:^{}];
        [self.tSDK infoCallBack];
    }];
    
    //----Google/SignIn
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    //[GIDSignIn sharedInstance].delegate = self;
    
    //----FaceBook/SignIn
    [[FBSDKApplicationDelegate sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
    
    //----GooglePlaces API
    [GMSPlacesClient provideAPIKey:@"AIzaSyApDqsQOKAJG-UInskQqrypuvtfKh0ypnk"];
    
    //----Use FIREBASE library to configure APIs
    [FIRApp configure];
    [FIRMessaging messaging].remoteMessageDelegate = self;
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [application registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
            #if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert | UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            #endif
        }
    [application registerForRemoteNotifications];

    if([LoginController check_SharedPreferences])
        [Utilities skipLoginController:NO];
    else { //load Onboarding screen buttons
        UIPageControl *pageControl = [UIPageControl appearance];
        pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        pageControl.currentPageIndicatorTintColor = [Utilities colorWithHexString:secondary];
        return YES;
    }
    
    //-----Continuous check for INTERNET reachability
    Reachability *internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    [self checkNetworkStatus:nil];  //On app start, check internet connectivity as well
    
    //WaveAnimation
    //[self initializeWaveViewController];//Now Initialized in viewDidLoad of HomeController
    //------Splash Screen
    [self splashScreen];
    return YES;
}

-(void)splashScreen{
    splashScreen *ss = [[splashScreen alloc]init];
    
    [self.window.rootViewController.view addSubview:ss.view];
    
    //now fade out splash image
    // Delay execution of my block for 10 seconds.
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 6 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [UIView transitionWithView:self.window duration:0.7f options:UIViewAnimationOptionTransitionNone
                        animations:^(void){
                            ss.view.alpha=0.0f;
                        } completion:^(BOOL finished){
                            [ss.view removeFromSuperview];
        }];
    });
    
  

}

- (void)checkNetworkStatus:(NSNotification *)notice {
    // called after network status changes
    Reachability *internetReachable = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus) {
        case NotReachable:{
            NSLog(@"Listener: The internet is down.");
            break;
        }
        case ReachableViaWiFi:{
            NSLog(@"Listener: The internet is Connected.");
            [Utilities syncOnInternet];
            break;
        }
        case ReachableViaWWAN: {
            NSLog(@"Listener: The internet is working via WWAN!");
            [Utilities syncOnInternet];
            break;
        }
    }//@end switch
}//@end FUNCTION


//---------------------
#pragma mark - Firebase Delegate methods
//---------------------
//-----With "FirebaseAppDelegateProxyEnabled": NO
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSLog(@"\nEntered Appdelegate/didRegisterForRemoteNotificationsWithDeviceToken ");
    [[FIRInstanceID instanceID ] setAPNSToken:deviceToken type:FIRInstanceIDAPNSTokenTypeSandbox ];
    [self connectToFCM];
}

-(void)connectToFCM {
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
        }
    }];
}

-(void)sendFCMtoServer:(NSString *)usertoken{
    NSString *fcmToken = [[FIRInstanceID instanceID]token];
    NSLog(@"\n -----FCM token is: %@\n...", fcmToken);
    NSURL *url = [NSURL URLWithString: URLnotify];
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[NSString stringWithFormat:@"%@", fcmToken] forKey:@"fcm_key"];
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:usertoken forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    NSLog(@"FCM dictionary : %@",dict);
    [Utilities sendRequestToServer:request message:@"Appdelegate/ sendFCMtoServer" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\nFCM response : %@ \n...", responseJSON);
    }];
}

//-----[START refresh_token]
- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    [self connectToFCM];
    NSLog(@"\n -----[START refresh_token FCM]:\nFCM registration token is: %@\n...", fcmToken);
#pragma mark SharedPreferences
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *usertoken = [preferences objectForKey:@"usertoken"];
    if(usertoken != nil)
        [self sendFCMtoServer:usertoken];
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

//----[START receive_Notification and then method called on click]
//----Note: If you want a notification to be displayed on the app: SEND PARTICULAR JSON AS IN IOS FCM DOCS
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
     [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    // Print full message.
    NSLog(@"0. %@", userInfo);
}
//-----With "FirebaseAppDelegateProxyEnabled": NO
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler: (void (^)(UIBackgroundFetchResult))completionHandler
{
    // Let FCM know about the message for analytics etc.
    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    NSLog(@"\n\n1. Recieved msg : %@ \n\n.", userInfo);
    // handle your message.
}

//------Recieves message in foreground APP
- (void)applicationReceivedRemoteMessage: (nonnull FIRMessagingRemoteMessage *)remoteMessage{
    NSLog(@"\n\n2. Recieved msg : %@ ", remoteMessage.appData);

    NSString *payload = [remoteMessage.appData valueForKey:@"database"];
    NSData *data = [payload dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"\n---database : %@ \n\n.", json);
    
    //[self.DB saveApiResults:json];  //--Saving In Database
    
    //----Sending ack
    NSString *sdk_token = [[NSUserDefaults standardUserDefaults] objectForKey:@"sdk_token"];
    NSString *url_string = [NSString stringWithFormat:@"%@\/noti\/ack\/",URLhit];
    NSURL *url = [NSURL URLWithString: url_string];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"GET"];
    [request setValue:sdk_token forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    //---Whatever the notification, LOAD database again to get latest value from get_all_inof and stores
    [self.DB loadDatabase:^{
        [Utilities sendRequestToServer:request message:@"(FCM ack)Appdelegate/ applicationReceivedRemoteMessage" withResponseCallback:^(NSDictionary *responseJSON) {
            NSLog(@"\n\nACK response : %@ \n...", responseJSON);
        }];
    }];
}


- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
    NSLog(@"3. Received data message: %@", remoteMessage.appData);
}

//--Called when a notification is delivered to a foreground app.
-(void) :(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

//--Called to let your app know which action was selected by the user for a given notification.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center 
didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
}



//------------------------------------------------------------------------------
#pragma mark - Google/SignIn
//------------------------------------------------------------------------------

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary *)options {
    if ([[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:options[UIApplicationOpenURLOptionsAnnotationKey]])
        return YES;
    else if([[FBSDKApplicationDelegate sharedInstance] application:app
            openURL:url
            sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
            annotation:options[UIApplicationOpenURLOptionsAnnotationKey]])
        return YES;
    
    return NO;
}



- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}

//------------------------------------------------------------------------------
#pragma mark - Misc
//------------------------------------------------------------------------------


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ForegroundHome" object:nil userInfo:nil];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//------------------------------------------------------------------------------
#pragma mark - Device details
//------------------------------------------------------------------------------
-(NSString*)device_name {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform= [NSString stringWithCString:systemInfo.machine
                                           encoding:NSUTF8StringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 GSM";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s GSM";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPhone8,4"])    return @"iPhone SE";
    if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}


//---------------------
#pragma mark - Recording CallBack Method
//---------------------

-(void)onToneDecrypted:(int)finalCode : (bool)isLocation{
    NSDictionary *tableData = [self.DB fetchCodeDictionary:finalCode islocation:isLocation];
    NSLog(@"onToneDecrypted: %d",finalCode);
    // Issue vibrate
    AudioServicesPlaySystemSound(1352);
    
    if([tableData count]==0)
        return;
    else if (isLocation==false && [Utilities checkContest:tableData withView:self.window.rootViewController]==YES){
        return;
    }
    
    //---Check if token is_seen or NOT
    BOOL location_show = NO;
    int is_seen = 0;
    NSString *zone_timestamp = nil;
    NSString *region_id =nil;
    if(isLocation == false) //---Check COUPON if is_seen if 1 or not
        for(int i=0; i<[tableData count] ; i++){
            NSString *key = [NSString stringWithFormat:@"%d",i];
            NSDictionary *dict = [tableData valueForKey:key];
            is_seen = [[dict valueForKey:@"is_seen"] integerValue];
            break; //Just check one coupons is_seen value
        }
    else if(isLocation == true){
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];//SET NSUserDefaults
        zone_timestamp = [preferences objectForKey:@"zone_timestamp"];
        NSString *regionID = [preferences objectForKey:region_identity];
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        //---See zone id
        NSString *zone_id;
        for(int i=0; i<[tableData count] ; i++){
            NSString *key = [NSString stringWithFormat:@"%d",i];
            NSDictionary *dict = [tableData valueForKey:key];
            zone_id = [dict valueForKey:@"zone_id"];
        }
        
        if(zone_timestamp==nil || ![regionID isEqualToString:zone_id]){
            //--Save to preferences.. convert date to string
            NSDate *date_today = [NSDate date];
            NSString *stringFromDate = [dateFormat stringFromDate:date_today];
            [preferences setObject:stringFromDate forKey:@"zone_timestamp"];
            const BOOL didSave = [preferences synchronize];
            location_show = YES;
        } else {
            //--convert string to date
            NSDate *date_today = [NSDate date];
            NSDate *expiry_date = [dateFormat dateFromString:zone_timestamp];
            NSTimeInterval secondsBetween = [date_today timeIntervalSinceDate:expiry_date];
            int numberOfHours= secondsBetween/3600;
            if(numberOfHours>3){
                location_show = YES;
                [preferences removeObjectForKey:@"zone_timestamp"];
            }
            else location_show = NO;
        }
    }//@end else if isLocation==true
    
    //-----Popping open the card
    if(isLocation==false && is_seen==0 ){
        // Issue vibrate
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        // Notification to add Badge on Offers
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OfferNotification" object:nil userInfo:nil];
        [self.wVC ChangeSwiftFlagWithTflag:1 completionHandler:^{
            //--Update Table is_seen as 1
            for(int i=0; i<[tableData count] ; i++){
                NSString *key = [NSString stringWithFormat:@"%d",i];
                NSDictionary *dict = [tableData valueForKey:key];
                NSString *unique_id = [dict valueForKey:@"id"];
                [self.DB updateColumnofTable:@"coupon_info" withColumnName:@"is_seen" unique_id:unique_id andValue:@"1" isInteger:YES];
            }
            [Utilities cardPopOpen:tableData withButton:YES andViewController:self.window.rootViewController]; //---cardPopUpn
        }]; //@end of completion block
    }
    else if(isLocation==true && location_show==YES){
        // Issue vibrate
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        
        [self.wVC ChangeSwiftFlagWithTflag:1 completionHandler:^{
            [Utilities cardZone:tableData andViewController:self.window.rootViewController]; //---cardPopUpn
            //----
            #pragma mark Home Progress bar
            self.progressBarShow = YES;
        }]; //@end of completion block
        
        #pragma mark SharedPreferences region
        NSString *zone_id;
        for(int i=0; i<[tableData count] ; i++){
            NSString *key = [NSString stringWithFormat:@"%d",i];
            NSDictionary *dict = [tableData valueForKey:key];
            zone_id = [dict valueForKey:@"zone_id"];
        }
        [Utilities setRegionSharedPreference:zone_id];
    }
    else if(isLocation==true && location_show==NO){
        NSString *zone_id;
        for(int i=0; i<[tableData count] ; i++){
            NSString *key = [NSString stringWithFormat:@"%d",i];
            NSDictionary *dict = [tableData valueForKey:key];
            zone_id = [dict valueForKey:@"zone_id"];
        }
        #pragma mark SharedPreferences region
        if (zone_id != nil) [Utilities setRegionSharedPreference:zone_id];
    }

}

-(void)onDataRecieved: (int) finalCode : (NSDictionary*)payload {
    
}


//--------------------------------------------
#pragma mark - Swift Connector Wave
//---------------------------------

-(void) initializeWaveViewController {
    UIViewController *current_vc = self.window.rootViewController; //cURRENT view
    CGFloat heightOfWave =current_vc.view.frame.size.height/10;
    UIView *temp_view = [[UIView alloc] initWithFrame:CGRectMake(current_vc.view.frame.origin.x- current_vc.view.frame.size.width/4, current_vc.view.frame.size.height/2-heightOfWave/2, current_vc.view.frame.size.width/10, heightOfWave)];
    //---Swift class
    self.wVC = [[WaveViewController alloc] init];
    [self.wVC ChangeSwiftFlagWithTflag:0 completionHandler:^{}];
    //--
    [current_vc addChildViewController:self.wVC]; //Add child viewcontroller
    [current_vc.view addSubview:self.wVC.view];   //Add child's view as subview
    self.wVC.view.frame = temp_view.frame;        //Add child's frame property
    [self.wVC didMoveToParentViewController:current_vc]; //Passing the reference to the parentcontroller.
}

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"trillZone.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"coreDataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSURL *)applicationDocumentsDirectory{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                   inDomains:NSUserDomainMask] lastObject];
}



@end
