//
//  UnlockedRecentlyView.h
//  trillZone
//
//  Created by Shivansh on 2/14/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UnlockedRecentlyView : UIViewController

@property (nonatomic, strong) AppDelegate *appDelegate;

@end
