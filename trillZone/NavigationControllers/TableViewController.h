//
//  TableViewController.h
//  trillZone
//
//  Created by Shivansh on 11/6/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <trillSDK/Database.h>
#import "SWRevealView.h"
#import "FirstController.h"

@interface TableViewController : UITableViewController <UIPageViewControllerDataSource>

@property (nonatomic, strong) AppDelegate *appDelegate;
//--Form
@property(nonatomic, strong) UIViewController *vc_temp;
@property(nonatomic, strong) UITextField *textfield;
@property(nonatomic, strong) UITextView *textview;
//---HELP section 
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@end
