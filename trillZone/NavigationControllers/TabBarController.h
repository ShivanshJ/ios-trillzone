//
//  TabBarController.h
//  trillZone
//
//  Created by Shivansh on 1/31/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"

@interface TabBarController : UITabBarController <UITabBarControllerDelegate>

@end
