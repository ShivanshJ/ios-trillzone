//
//  TableViewController.m
//  trillZone
//
//  Created by Shivansh on 11/6/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import "TableViewController.h"

@interface TableViewController ()
{
    NSMutableArray *tableData;
    NSMutableArray *imageData;
}
@end

@implementation TableViewController

@synthesize appDelegate;
@synthesize vc_temp, textfield, textview;

@synthesize pageTitles,pageImages; //for HELP section

- (void)viewDidLoad{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //self. alwaysBounceVertical = NO;
    
//    tableData = [[appDelegate DB] displayTable:@"coupon_info" selectfromTableNamePLUSCondition:nil]; //[DB displayTable:   columnName: @"brand_name"];
    tableData = @[@"Account", @"Coupon History", @"Help", @"Log Out"];
    imageData = @[@"menu_account",@"menu_coupon",@"menu_help",@"menu_logout"];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = YES;
    
    //---Properties
    self.tableView.backgroundColor = [Utilities colorWithHexString:black_one];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0);
}

- (void)viewDidAppear:(BOOL)animated{
//    NSLog(@"TableViewController - TableData %@", tableData);
    
}

//----------------------------
#pragma mark - Table view data source
//----------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
 return [tableData count];
}

#pragma mark - Insert values in cell
//---Insert values in each cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
//    NSString *val = [NSString stringWithFormat:@"%d",indexPath.row];
//    NSDictionary *dict = [tableData valueForKey:val];
//    NSString *values = [dict valueForKey:@"brand_name"];
//    cell.textLabel.text = values;
    cell.textLabel.text = [tableData objectAtIndex:indexPath.row];
    cell.imageView.image =  [UIImage imageNamed:[imageData objectAtIndex:indexPath.row]];
    
    //----Customize text properties
    cell.backgroundColor = [Utilities colorWithHexString:black_one];
    cell.textLabel.textColor = [Utilities colorWithHexString:@"fffff"];
    cell.textLabel.font = [UIFont fontWithName:@"Raleway-SemiBold" size:16];
    
    //--Add Button
//    UIButton *button = [[UIButton alloc] init];
//    cell.butt

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
#pragma mark Account
    if(indexPath.row == 0){
        UIViewController *vc_temp = [[UIViewController alloc] init];//--viewcontroller
        vc_temp.view.backgroundColor = [Utilities colorWithHexString:black_one];
        //---Push with SWReveal
        UITabBarController *tbc = (UITabBarController *)self.revealViewController.frontViewController;
        UINavigationController *nc = tbc.selectedViewController;
        [nc pushViewController:vc_temp animated:NO];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        [tbc.tabBar setHidden:YES];
        //---Nav Bar props
        nc.navigationBar.tintColor = [Utilities colorWithHexString:@"ffffff"];
        nc.navigationBar.barTintColor = [Utilities colorWithHexString:secondary];
        CGFloat leaveTopHeight = 0;//nc.navigationBar.frame.size.height + 30;
        //UIView *solidcolor = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, vc_temp.view.frame.size.width, leaveTopHeight)];
        //solidcolor.backgroundColor = [Utilities colorWithHexString:black_one];
        //[vc_temp.view addSubview:solidcolor];
        
        //----Fetch Shared Preferences and display
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        NSString *userEmail = [preferences objectForKey:user_email];
        NSString *userName = [preferences objectForKey:user_name];
        NSString *userImageURL = [preferences objectForKey:user_imageURL];
        NSString *trill_point = [preferences objectForKey:trill_points];

        //---Drawing Circle
        CGFloat circleSize = 120;
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, vc_temp.view.frame.size.width/4, circleSize,  circleSize)];
        imageView.center = CGPointMake(vc_temp.view.frame.size.width  / 2,
                                       vc_temp.view.frame.size.height / 3);
        imageView.contentMode = UIViewContentModeCenter;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [imageView sd_setImageWithURL:[NSURL URLWithString:userImageURL] placeholderImage:[UIImage imageNamed:@"back_no_trill"]];
        imageView.layer.cornerRadius = circleSize/2;
        imageView.clipsToBounds = YES;
        imageView.layer.borderWidth = 3.0f;
        imageView.layer.borderColor = [[Utilities colorWithHexString:@"1b9fc6"] CGColor];
        //---Solid colour
        UIView *solidcolor2 = [[UIImageView alloc]init];
        solidcolor2.frame = CGRectMake(0, 0, vc_temp.view.frame.size.width, imageView.center.y);
        solidcolor2.backgroundColor = [Utilities colorWithHexString:@"1b9fc6"];
        [vc_temp.view addSubview:solidcolor2];
        
        [vc_temp.view addSubview:imageView];
        
        //---Adding Text
        UIView *EXTRA = [[UIView alloc]initWithFrame:CGRectMake(0, imageView.frame.origin.y + circleSize , vc_temp.view.frame.size.width, 300)];
//        EXTRA.backgroundColor = [UIColor redColor];
        UILabel *title = [[UILabel alloc]init];
        title.frame = CGRectMake(0, circleSize/2, EXTRA.frame.size.width, 38);
        title.text = userName;
        title.font = [UIFont fontWithName:@"Bebas" size:32];
        title.textColor = [Utilities colorWithHexString:@"ffffff"];
        title.textAlignment = NSTextAlignmentCenter;
        title.minimumScaleFactor = 0.5;
        title.adjustsFontSizeToFitWidth = YES;
        [EXTRA addSubview:title];
        //---Adding Subtitle
        UILabel *subtitle = [[UILabel alloc]init];
        subtitle.frame = CGRectMake(0, title.frame.origin.y+circleSize/2, title.frame.size.width, 100);
        subtitle.text = [NSString stringWithFormat:@"%@\n\nTrill Points : %@",userEmail,trill_point];
        subtitle.numberOfLines = 3;
        subtitle.font = [UIFont fontWithName:@"Raleway-Bold" size:18];
        subtitle.textColor = [Utilities colorWithHexString:secondary];
        subtitle.textAlignment = NSTextAlignmentCenter;
        subtitle.minimumScaleFactor = 0.5;
        subtitle.adjustsFontSizeToFitWidth = YES;
        [EXTRA addSubview:subtitle];
        
        [vc_temp.view addSubview:EXTRA];
      
    }
#pragma mark Coupon History
    else if(indexPath.row == 1){
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"CouponView"];
        
        UITabBarController *tbc = (UITabBarController *)self.revealViewController.frontViewController;
        UINavigationController *nc = tbc.selectedViewController;
        [nc pushViewController:vc animated:NO];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
    }
#pragma mark HELP
    else if(indexPath.row == 2){
        
        self.vc_temp = [[UIViewController alloc] init];//--viewcontroller
        self.vc_temp.view.backgroundColor = [Utilities colorWithHexString:black_one];
        //---Push with SWReveal
        UITabBarController *tbc = (UITabBarController *)self.revealViewController.frontViewController;
        UINavigationController *nc = tbc.selectedViewController;
        [nc pushViewController:self.vc_temp animated:NO];
        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        [tbc.tabBar setHidden:YES];
        //---Nav Bar props
        nc.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
        nc.navigationBar.barTintColor = [Utilities colorWithHexString:black_one];
        
        //--------Page ViewController
        self.pageTitles = @[@"Listen to Audio", @"Recieve the best offers", @"Earn trill Points & Win"];
        self.pageImages = @[@"page1", @"page2", @"page5"];
        UIPageViewController *pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"OnboardViewController"];;
        pageViewController.view.frame = CGRectMake(0, 0, self.vc_temp.view.frame.size.width, self.vc_temp.view.frame.size.height);
        pageViewController.dataSource = self;
        PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
        NSArray *viewControllers = @[startingViewController];
        [pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
        
        [self.vc_temp addChildViewController:pageViewController];
        [self.vc_temp.view addSubview:pageViewController.view];
        [pageViewController didMoveToParentViewController:self.vc_temp];
//
//        UITabBarController *tbc = (UITabBarController *)self.revealViewController.frontViewController;
//        UINavigationController *nc = tbc.selectedViewController;
//        [nc pushViewController:vc animated:NO];
//        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
        
//        self.vc_temp = [[UIViewController alloc] init];//--viewcontroller
//        self.vc_temp.view.backgroundColor = [Utilities colorWithHexString:@"1b9fc6"];
//        //---Push with SWReveal
//        UITabBarController *tbc = (UITabBarController *)self.revealViewController.frontViewController;
//        UINavigationController *nc = tbc.selectedViewController;
//        [nc pushViewController:self.vc_temp animated:NO];
//        [self.revealViewController setFrontViewPosition:FrontViewPositionLeft animated:YES];
//        //---Nav Bar props
//        nc.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
//        CGFloat leaveTopHeight = nc.navigationBar.frame.size.height + 30;
//        UIView *solidcolor = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.vc_temp.view.frame.size.width, leaveTopHeight)];
//        solidcolor.backgroundColor = [Utilities colorWithHexString:black_one];
//        [self.vc_temp.view addSubview:solidcolor];
//        [tbc.tabBar setHidden:YES];
//
//        //---Form
//        //---Making View
//        UIView *popOverCoupon = [[UIView alloc] initWithFrame:self.vc_temp.view.frame];
//        UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
//        opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
//        opaqueImage.layer.opacity = 0.75;
//        [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
//
//        //---CardView
//        UIImageView *card = [[UIImageView alloc] initWithFrame:CGRectMake(20, popOverCoupon.frame.size.height/7, popOverCoupon.frame.size.width-40, popOverCoupon.frame.size.height*5/7)];
//        card.backgroundColor = [Utilities colorWithHexString:@"1e1e1e"];
//        //-Shadow for card
//        UIImageView *shadowView = [[UIImageView alloc] initWithFrame:card.frame];
//        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
//        shadowView.layer.masksToBounds = NO;
//        shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
//        shadowView.layer.shadowOffset = CGSizeMake(2.0f, 3.0f);
//        shadowView.layer.shadowOpacity = 0.9f;
//        shadowView.layer.shadowPath = shadowPath.CGPath;
//        shadowView.layer.shadowRadius = 1.0;
//        [popOverCoupon addSubview:shadowView];//--Add shadow on popOverView
//        //-Image on card
//        UIImageView *card_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, card.frame.size.width, card.frame.size.height/7)];
//        card_image.image = [UIImage imageNamed:@"back_no_trill"];
//        card_image.contentMode = UIViewContentModeCenter;
//        card_image.contentMode = UIViewContentModeScaleAspectFill;
//        card_image.clipsToBounds = YES;
//        [card addSubview:card_image];
//        //-Text TITLE
//        self.textfield= [[UITextField alloc] initWithFrame:CGRectMake(16, card_image.frame.size.height + 8, card_image.frame.size.width - 24, card_image.frame.size.height*4/5)];
//
//        self.textfield.delegate = self; //For the keyboard delegate
//
//        self.textfield.borderStyle = UITextBorderStyleNone;
//        self.textfield.backgroundColor = [UIColor whiteColor];
//        self.textfield.placeholder = @"Write your email address";
//        self.textfield.keyboardType = UIKeyboardTypeEmailAddress;
//        self.textfield.font = [UIFont fontWithName:@"Raleway-Light" size:15];
//        self.textfield.textColor = [Utilities colorWithHexString:@"1e1e1e"];
//        self.textfield.returnKeyType = UIReturnKeyDefault;
//        self.textfield.clearButtonMode = UITextFieldViewModeWhileEditing;
//        self.textfield.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
//        self.textfield.delegate = self;
//        //        title.contentMode = UIViewContentModeCenter;
//        self.textfield.adjustsFontSizeToFitWidth = YES;
//        [card addSubview:self.textfield];
//        [card setUserInteractionEnabled:YES];
//        //-Description DESCP
//        self.textview= [[UITextView alloc] initWithFrame:CGRectMake(self.textfield.frame.origin.x, card_image.frame.size.height+ self.textfield.frame.size.height +20, self.textfield.frame.size.width, card.frame.size.height/2)];
//
//        self.textview.delegate = self; //For the keyboard delegate
//        self.textview.font = [UIFont fontWithName:@"Raleway-Regular" size:16];
//        self.textview.textColor = [Utilities colorWithHexString:secondary];
//        self.textview.backgroundColor = [UIColor whiteColor];
//        self.textview.keyboardType = UIKeyboardTypeDefault;
//        self.textview.font = [UIFont fontWithName:@"Raleway-Bold" size:15];
//        self.textview.contentMode = UIViewContentModeTop;
//        [card addSubview:self.textview];
//        //--AddinUIButton
//        UIButton *button_send = [[UIButton alloc] initWithFrame:CGRectMake(self.textview.frame.origin.x, self.textview.frame.origin.y + self.textview.frame.size.height + 10, self.textview.frame.size.width ,50)];
//        UIView *buttonView = [[UIView alloc] initWithFrame:button_send.frame];
//        UILabel *label = [[UILabel alloc] initWithFrame:buttonView.frame];
//        [buttonView addSubview:label];
//        button_send.titleLabel.text = @"BUTTON";
//        button_send.titleLabel.font = [UIFont fontWithName:@"Raleway-Regular" size:16];
//        button_send.titleLabel.textColor = [Utilities colorWithHexString:secondary];
//        [button_send addTarget:self action:@selector(FormButtonPressSelector:) forControlEvents:UIControlEventTouchUpInside];
//        [Utilities colorDrawViewGradientLeft:@"fc4a1a" Right:@"f7b733" andSelfDotView:buttonView];
//        [card addSubview:buttonView];
//        UILabel *button_heading = [[UILabel alloc]initWithFrame:buttonView.frame];
//        button_heading.text = @"SEND";
//        button_heading.textAlignment = NSTextAlignmentCenter;
//        button_heading.font = [UIFont fontWithName:@"Raleway-Bold" size:16];
//        button_heading.textColor = [UIColor whiteColor];
//        [card addSubview:button_heading];
//        [card addSubview:button_send];
//
//        [popOverCoupon addSubview:card]; //--Add card on popOverView
//        [popOverCoupon setUserInteractionEnabled:YES];
//        [self.vc_temp.view addSubview:popOverCoupon];
    }
#pragma mark LOGOUT
    else if(indexPath.row == 3){
        
        if([[GIDSignIn sharedInstance] hasAuthInKeychain]!=nil)
            [[GIDSignIn sharedInstance] signOut];
        else if([FBSDKAccessToken currentAccessToken]!=nil)
            [FBSDKAccessToken setCurrentAccessToken:nil];
        #pragma mark Deleting Shared Preference
        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
        [preferences removeObjectForKey:@"usertoken"];
        [preferences removeObjectForKey:@"zone_timestamp"];
        
//        UINavigationController *navigationController = (UINavigationController*) [appDelegate window].rootViewController;
//        [navigationController popToRootViewControllerAnimated:YES];
        
        UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        [self.revealViewController revealToggleAnimated:YES];
        
        UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"LoginID"];
        [appDelegate window].rootViewController = vc;
        
        //---Deleting is_seen 1
        [[appDelegate DB] updateTableResetIS_SEEN];
        [self.revealViewController dismissViewControllerAnimated:NO completion:^{}];
    }
}

-(void)FormButtonPressSelector:(id)sender{
    NSString *API_SEND_HELP = [NSString stringWithFormat:@"%@\/user\/support\/", URLhit ];
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *usertoken = [preferences objectForKey:@"usertoken"];
    
    NSURL *url = [NSURL URLWithString: API_SEND_HELP];
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:[NSString stringWithFormat:@"%@", self.textfield.text] forKey:@"email"];
    [dict setValue:[NSString stringWithFormat:@"%@", self.textview.text] forKey:@"message"];
    
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:usertoken forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    (@"\n\n--NavigationControllers/TableViewController.m/FormButtonPressSelector : %@",dict);
    [Utilities sendRequestToServer:request message:@"TableViewController.m/FormButtonPressSelector" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\nResponse : %@ \n...", responseJSON);
    }];
    
}


-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range
 replacementText:(NSString *)text
{
    // Any new character added is passed in as the "text" parameter
    if ([text isEqualToString:@"\n"]) {
        // Be sure to test for equality using the "isEqualToString" message
        [textView resignFirstResponder];
        [self.vc_temp.view setFrame:CGRectMake(0,0,320,460)];
        // Return FALSE so that the final '\n' character doesn't get added
        return FALSE;
    }
    // For any other character return TRUE so that the text gets added to the view
    return TRUE;
}


-(BOOL)textViewShouldBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    return YES;
}


- (BOOL)textViewEndEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    [self.vc_temp.view endEditing:YES];
    return YES;
}


- (void)keyboardDidShow:(NSNotification *)notification
{
    // Assign new frame to your view
    [self.vc_temp.view setFrame:CGRectMake(0,0,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
    
}

-(void)keyboardDidHide:(NSNotification *)notification
{
    [self.vc_temp.view setFrame:CGRectMake(0,0,320,460)];
}


//-----------------------------------------------
#pragma mark - PageViewController for HELP
//-----------------------------------------------

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == [self.pageTitles count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.pageTitles count] == 0) || (index >= [self.pageTitles count])) {
        return nil;
    }
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController ;
        pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
        pageContentViewController.imageFile = self.pageImages[index];
        pageContentViewController.titleText = self.pageTitles[index];

    pageContentViewController.pageIndex = index;
    return pageContentViewController;
}


- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return [self.pageTitles count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

#pragma mark - Table delegates
//--------------Add IMAGE on header
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *imageView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.height/30)];
//    UIImageView *iv = [[UIImageView alloc] initWithFrame:imageView.frame];
//    iv.backgroundColor = [Utilities colorWithHexString:@"1b9fc6"];
//    [imageView addSubview:iv];
//    [self.view addSubview:imageView];
//    return imageView;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;//2*self.tableView.frame.size.height/30 ;
}

//--ceLL Height
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}




//----------------------------------------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
