//
//  TabBarController.m
//  trillZone
//
//  Created by Shivansh on 1/31/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "TabBarController.h"

@interface TabBarController ()

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIImage *tabBarBackground = [UIImage imageNamed:@"CustomUITabbar.png"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setSelectionIndicatorImage: [UIImage imageNamed:@"CustomUITabbarSelected.png"]];
    
    self.tabBar.translucent = YES;
    
    UITabBarController *tab=self.tabBarController;
    [self setSelectedIndex:2];

    // Add an observer - Offer incoming
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationOfferIncoming:) name:@"OfferNotification" object:nil];
}

#pragma mark - Notification
- (void)NotificationOfferIncoming:(NSNotification *) notification {
    //---Badge Notification
    [self addBadegOnIndex:0];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addBadegOnIndex:(int)index {
    for(UIView *subview in self.tabBar.subviews){
        if(subview.tag==0){
            subview.removeFromSuperview;
            break;
        }
    }
    CGFloat radius = 5;
    CGFloat topMargin = 5;
    CGFloat itemCount = [self.tabBar.items count];
    CGFloat HalfwidthOfEachItem = CGRectGetWidth(self.view.bounds) / (itemCount * 2);
    CGFloat xOffset = HalfwidthOfEachItem * (index * 2 + 1);
    
    CGFloat imageHalfWidth= self.tabBar.items[index].selectedImage.size.width / 2;
    UIView *Dot = [[UIView alloc] initWithFrame: CGRectMake( xOffset + imageHalfWidth,  topMargin,  radius*2, radius*2)];
    
    Dot.tag =1000+index;
    Dot.backgroundColor = [Utilities colorWithHexString:secondary];
    Dot.layer.cornerRadius = radius;
    
    [self.tabBar addSubview:Dot];
}


-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    [[self.view viewWithTag:(item.tag+1000)]removeFromSuperview];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
