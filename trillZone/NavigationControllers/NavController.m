//
//  NavController.m
//  trillZone
//
//  Created by Shivansh on 1/31/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "NavController.h"

@interface NavController ()

@end

@implementation NavController

@synthesize appDelegate,textField,placesClient, searchDropDown;
UIBarButtonItem *leftButton;
UIBarButtonItem *leftButton2;
UIBarButtonItem *rightButton;
UIBarButtonItem *backBarBtnItem;
int segment_number=0; //For segment control in search option

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Add an observer that will respond to loginComplete
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(NotificationRegionChange:) name:@"UserDefaultRegion" object:nil];
    
    //--For transparent navBar
    //[self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    //self.navigationBar.shadowImage = [UIImage new];
    self.navigationBar.translucent =  NO;
    self.navigationBar.barTintColor = [Utilities colorWithHexString:bar_primary]; //--- 20 shades darker than the actual colour , if RGB = 30,30,30 then for navbar RGB =10,10,10
    //self.navigationBar.backgroundColor = [Utilities colorWithHexString:@"0A0A0A"];
    

    //------Left Location Setting Button
    NSString *region = [Utilities getRegionNameWithID:[[NSUserDefaults standardUserDefaults] objectForKey:@"region"]];
    [self setLeftBarButton:region];
    [self setRightBarButton];
    
    //-----Adding to navigationBar Items
    [self.navigationItem setLeftBarButtonItems:@[leftButton,leftButton2]];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    [self.navigationBar pushNavigationItem:self.navigationItem animated:NO];
    
    //For touches began method
    self.searchDropDown.userInteractionEnabled =YES;
    _dropDown.userInteractionEnabled =YES;
    
    //-----SWRevealViewController (if only one item is there on leftButton, targets and actions can be set in this way too)
//    SWRevealViewController *revealViewController = self.revealViewController;
//    if ( revealViewController ) {
//        [self.navigationItem.leftBarButtonItem setTarget: self.revealViewController];
//        [self.navigationItem.leftBarButtonItem setAction: @selector( revealToggle: )];
//        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
//    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:NO];
    NSLog(@"\nAppear Nav...");
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    NSLog(@"gone Nav...");
}

//-(void) viewDidDisappear:(BOOL)animated{
//    [_dropDown removeFromSuperview];
//    [self.view removeFromSuperview];
//}


#pragma mark - Notification Location change
//NOTE: Notification runs everytime for NavBar of each TAB
- (void) NotificationRegionChange:(NSNotification *) notification
{
    if ([[notification name] isEqualToString:@"UserDefaultRegion"]){
        NSLog (@"NavController.. received the regionChange notification!");
        NSDictionary* userInfo = notification.userInfo;
        NSString *regionID = [userInfo objectForKey:@"region"];
        NSString *regionName = [Utilities getRegionNameWithID:regionID];
        [self setLeftBarButton:regionName];
        [self.navigationItem setLeftBarButtonItems:@[leftButton,leftButton2]];
    }
}

-(void)setRightBarButton{
    UIButton *searchButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    //button.backgroundColor = [UIColor whiteColor].CGColor;
    [searchButton setFrame:CGRectMake(0, 0, self.navigationBar.frame.size.height, self.navigationBar.frame.size.height)];
    [searchButton addTarget:self action:@selector(didTapSearchButton:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat percent = 70;
    CGFloat size= self.navigationBar.frame.size.height*(percent/100);
    CGFloat size2= self.navigationBar.frame.size.height*(1 - percent/90);
    UIImageView *button_image= [[UIImageView alloc] initWithFrame:CGRectMake(size2+7, size2, size, size)];

    button_image.image = [UIImage imageNamed:@"ic_search"];
    button_image.contentMode = UIViewContentModeScaleAspectFit;
    button_image.tintColor = [UIColor whiteColor];
    [searchButton addSubview:button_image];  //---Add subview
    
    rightButton = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
}

-(void)setLeftBarButton:(NSString*)region{
    //------Left HamMenu Button
    leftButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"ham_menu"] style:UIBarStyleDefault target:self action:@selector(hamburgerMenu:)];
    [leftButton setTarget:self.revealViewController];
    [leftButton setAction: @selector( revealToggle: )];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    leftButton.tintColor = [Utilities colorWithHexString:@"63B8FF"];
    
    //---LeftButton2
    if(region == nil) region = @"Select Zone";
    UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
    //button.backgroundColor = [UIColor whiteColor].CGColor;
    [button setFrame:CGRectMake(0, 0, 162, 30)];
    [button addTarget:self action:@selector(didTapLocationButton:) forControlEvents:UIControlEventTouchUpInside];
    UILabel *labelHeader = [[UILabel alloc]initWithFrame:CGRectMake(3, 3, 125, button.frame.size.height)];
    [labelHeader setFont:[UIFont fontWithName:@"Raleway-SemiBold" size:18]];
    [labelHeader setText:region];
    [labelHeader setTextColor:[Utilities colorWithHexString:@"c2c2c2"]];
    labelHeader.textAlignment = UITextAlignmentLeft;
    [labelHeader setBackgroundColor:[UIColor clearColor]];
    [button addSubview:labelHeader];  //---Add subview
    UIImageView *button_image = [[UIImageView alloc] initWithFrame:CGRectMake(labelHeader.frame.size.width-5, 5, 32, 32)];
    button_image.tintColor = [Utilities colorWithHexString:@"F5AB35"];
    button_image.image = [UIImage imageNamed:@"ic_dropdown"];
    [button addSubview:button_image];  //---Add subview
    
    leftButton2 = [[UIBarButtonItem alloc] initWithCustomView:button];
}

//----------------------------------------
#pragma mark - TapLocation VC -
//----------------------------------------

-(IBAction)didTapLocationButton: (id)sender{
    NSLog(@"Navigation Bar: didTapLocationButton");
    //---AddUIViewController
    UIViewController *vc_temp = [[UIViewController alloc] init];    //-- VIEWCONTROLLER
    vc_temp.view.backgroundColor = [UIColor whiteColor];
    CGFloat navBarHeight = self.navigationBar.frame.size.height;
    //---AddScrollView
    UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, vc_temp.view.frame.size.width, vc_temp.view.frame.size.height)];
    [vc_temp.view addSubview:myScrollView]; //---addsubView SCROLLVIEW
    
    //---NavBar Colour
    self.navigationBar.barTintColor = [Utilities colorWithHexString:@"ffffff"];
    self.navigationBar.tintColor = [Utilities colorWithHexString:@"1b9fc6"];
    
    //----Search textField
    CGFloat leaveGapfromNavBar =20;
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(16, leaveGapfromNavBar, vc_temp.view.frame.size.width - 32, 30 )];
    #pragma mark SearchField as we type
    [self.textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    //[textField becomeFirstResponder];
    self.textField.delegate = self;
    self.textField.placeholder = @"Type mall name or location";
    self.textField.textColor = [Utilities colorWithHexString:@"1e1e1e"];
    self.textField.returnKeyType = UIReturnKeyDefault;
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5f;
    border.borderColor = [Utilities colorWithHexString:@"b5b5b5"].CGColor;
    border.frame = CGRectMake(0, self.textField.frame.size.height - borderWidth, self.textField.frame.size.width, self.textField.frame.size.height);
    border.borderWidth = borderWidth;
    [self.textField.layer addSublayer:border]; //---addSubLayer Line
    self.textField.layer.masksToBounds = YES;
    [myScrollView addSubview:self.textField]; //---addSubview SearchTextField
    //---E.g. Connought Place (label)
    CGRect frame = self.textField.frame;
    CGPoint point = CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
    UILabel *eg_label = [[UILabel alloc] initWithFrame:CGRectMake(16, point.y+2, vc_temp.view.frame.size.width-32, self.textField.frame.size.height-10)];
    eg_label.text = @"E.G. Orion Mall, Bengaluru";
    eg_label.font = [UIFont fontWithName:@"Raleway-Regular" size:9];
    eg_label.textColor = [Utilities colorWithHexString:@"1e1e1e"];
    [myScrollView addSubview:eg_label]; //---addSubview eg_Label
    
    //--------BUTTON GMSPlaceClient current Location
    self.placesClient = [GMSPlacesClient sharedClient];
    CGFloat gapFromSearchBar = point.y + 19*2;
    //-00 DrawLine
    UIView *Line1 = [[UIView alloc]initWithFrame:CGRectMake(16, gapFromSearchBar-2.5, self.view.frame.size.width -32, 1)];
    Line1.backgroundColor = [Utilities colorWithHexString:@"e0e0e0"];
    [myScrollView addSubview:Line1];            //--addSubView Line
    //-00
    UIButton *currentLocButton = [[UIButton alloc] initWithFrame:CGRectMake(16,gapFromSearchBar, vc_temp.view.frame.size.width-32, textField.frame.size.height)];
    [currentLocButton addTarget:self action:@selector(getCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
    [currentLocButton setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"eaeaea"]] forState:UIControlStateHighlighted];
    [currentLocButton setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"ffffff"]] forState:UIControlStateNormal];
        //-current Location Image
        UIImageView *loc_image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 4, 25, 25)];
        loc_image.image = [UIImage imageNamed:@"ic_current_loc"];
        [currentLocButton addSubview:loc_image];   //---ButtonSubview image
        //-current Location Label
        UILabel *loc_label = [[UILabel alloc] initWithFrame:CGRectMake(loc_image.frame.size.width+16, 4, vc_temp.view.frame.size.width-73, textField.frame.size.height-4)];
        loc_label.text = @"Select Nearest Mall";
        loc_label.font = [UIFont fontWithName:@"Raleway-SemiBold" size:15];
        loc_label.textColor = [Utilities colorWithHexString:@"1b9fc6"];
        [currentLocButton addSubview:loc_label]; //---ButtonSubview label
    [myScrollView addSubview:currentLocButton]; //---addSubview button
    //-00 DrawLine
    UIView *Line2 = [[UIView alloc]initWithFrame:CGRectMake(16, gapFromSearchBar+currentLocButton.frame.size.height+3.5, self.view.frame.size.width -32, 1)];
    Line2.backgroundColor = [Utilities colorWithHexString:@"e0e0e0"];
    [myScrollView addSubview:Line2];            //--addSubView Line
    //-00
    
    //----------Malls List
    CGRect locbuttonFrame = currentLocButton.frame;
    CGPoint locbuttonPoint = CGPointMake(CGRectGetMaxX(locbuttonFrame), CGRectGetMaxY(locbuttonFrame));
    [self showMallsList:vc_temp withScroller:myScrollView andPoint:locbuttonPoint];
    
    [self.tabBarController.tabBar setHidden:YES];
    [self pushViewController:vc_temp animated:YES];
}
//--------------------------------------------

- (void)textFieldDidChange:(NSNotification *)notification {
    [self searchForLocationWithText:self.textField.text];
}

//------Location autoComplete search
- (void)searchForLocationWithText:(NSString *)searchString{
    #pragma mark SearchMap Autocomplete
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterEstablishment;
    filter.country=@"IN";
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(13.12009956, 77.23613374);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(12.69576814,77.93788545);
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast coordinate:southWest];
    
    //---Clear the previous dropDown and redraw
    [_dropDown removeFromSuperview];
    _dropDown = [[UIView alloc] initWithFrame:CGRectMake(0, self.navigationBar.frame.size.height*3 + 60 , self.view.frame.size.width, self.view.frame.size.height)];
    
    [self.placesClient autocompleteQuery:searchString bounds:bounds filter:filter callback:^(NSArray *results, NSError *error) {
        if (error != nil) {
            NSLog(@"Autocomplete error %@", [error localizedDescription]);
            return;
        }
        _dropDown.backgroundColor = [UIColor whiteColor];
        CGFloat yPos =10;
        //---START LOOP
        for (GMSAutocompletePrediction* result in results) {
            //NSLog(@"Result '%@' with placeID %@", result.attributedFullText.string, result.placeID);
            //-----Button
            CGFloat fontSize = 17;
            CGFloat gap = 4;
            CGRect button_frame = CGRectMake(16, yPos, self.view.frame.size.width-32, fontSize+gap);
            yPos += fontSize + gap + 8;
            UICustomButton *button = [[UICustomButton alloc] initWithFrame:button_frame];
            button.unique_id = result.placeID;
            [button addTarget:self action:@selector(setLocationAutocompleteButton:) forControlEvents:UIControlEventTouchUpInside];
            [button setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"eaeaea"]] forState:UIControlStateHighlighted];
            [button setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"ffffff"]] forState:UIControlStateNormal];
                //---Creating Labels
                UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , self.view.frame.size.width-32, fontSize+gap)];
                label.text = result.attributedFullText.string;
                label.font =[UIFont fontWithName:@"Raleway-Regular" size:fontSize];
                label.textColor = [Utilities colorWithHexString:@"1e1e1e"];
                [button addSubview:label];         //--addSubView mallName
            //-00 DrawLine
            CGRect frame = button.frame;
            CGPoint point = CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
            UIView *Line = [[UIView alloc]initWithFrame:CGRectMake(16, point.y + 2, self.view.frame.size.width -32, 0.5)];
            Line.backgroundColor = [Utilities colorWithHexString:@"dadada"];
            [_dropDown addSubview:Line];            //--addSubView Line
            //-00
            [_dropDown addSubview:button];          //--addSubView BUTTON
        }//@end LOOP
        [self.view addSubview:_dropDown];
    }];//@end PLACESclient
}


//----tapButton to get current location and writes closest mall to sharedPreferences
- (IBAction)getCurrentLocation:(UIButton *)sender {
    sender.backgroundColor = [Utilities colorWithHexString:@"f2f200"];
    [self.placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *placeLikelihoodList, NSError *error){
        if (error != nil) {
            NSLog(@"Pick Place error %@", [error localizedDescription]);
            return;
        }
        textField.text = @"No current place";
        //self.addressLabel.text = @"";
        if (placeLikelihoodList != nil) {
            GMSPlace *place = [[[placeLikelihoodList likelihoods] firstObject] place];
            if (place != nil) {
                self.textField.text = place.name;
                [ViewController getClosestZoneLocation_ZoneID:place.coordinate.latitude :place.coordinate.longitude];   //-->Here ViewController is class Name of HomeController
                //Delay popping to viewcontroller
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.75 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [self popToRootViewControllerAnimated:YES];
                    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                    NSString *regionID = [preferences objectForKey:region_identity];
                    if([regionID isEqualToString:selectZoneID])
                        [Utilities showAlertWithTitle:@"No mall nearby" andMessage:@"Our malls are not within a radius of 1.5km of your current location"];
                });
            }
        }
    }];
}


//----------------------------------------
#pragma mark - TapSearch VC -
//----------------------------------------
-(IBAction)didTapSearchButton: (id)sender{
    NSLog(@"Navigation Bar: didTapSearchButton");
    //---AddUIViewController
    UIViewController *vc_temp = [[UIViewController alloc] init];    //-- VIEWCONTROLLER
    vc_temp.view.backgroundColor = [UIColor whiteColor];
    CGFloat navBarHeight = self.navigationBar.frame.size.height;
    //---Add a view
    UIView *myView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, vc_temp.view.frame.size.width, vc_temp.view.frame.size.height)];
    [vc_temp.view addSubview:myView]; //---addSUBVIEW myView
    
    //---NavBar Colour
    self.navigationBar.barTintColor = [Utilities colorWithHexString:@"ffffff"];
    self.navigationBar.tintColor = [Utilities colorWithHexString:@"1b9fc6"];
    
    //----Search textField
    CGFloat leaveGapfromNavBar =20;
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(16, leaveGapfromNavBar, vc_temp.view.frame.size.width - 32, 30 )];
#pragma mark SearchField as we type
    [self.textField addTarget:self action:@selector(searchFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.textField.delegate = self;
    self.textField.placeholder = @"Type brand name";
    self.textField.textColor = [Utilities colorWithHexString:@"1e1e1e"];
    self.textField.returnKeyType = UIReturnKeyDefault;
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5f;
    border.borderColor = [Utilities colorWithHexString:@"b5b5b5"].CGColor;
    border.frame = CGRectMake(0, self.textField.frame.size.height - borderWidth, self.textField.frame.size.width, self.textField.frame.size.height);
    border.borderWidth = borderWidth;
    [self.textField.layer addSublayer:border]; //---addSubLayer Line
    self.textField.layer.masksToBounds = YES;
    [myView addSubview:self.textField]; //---addSubview SearchTextField
    //---E.g. WOODLAnd (label)
    CGRect frame = self.textField.frame;
    CGPoint point = CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
    UILabel *eg_label = [[UILabel alloc] initWithFrame:CGRectMake(16, point.y+2, vc_temp.view.frame.size.width-32, self.textField.frame.size.height-10)];
    eg_label.text = @"E.G. Woodland";
    eg_label.font = [UIFont fontWithName:@"Raleway-Regular" size:9];
    eg_label.textColor = [Utilities colorWithHexString:@"1e1e1e"];
    [myView addSubview:eg_label]; //---addSubview eg_Label
    
    //-------Segment Control (COUPON SEARCH, STORE SEARCH)
    NSArray *itemArray = [NSArray arrayWithObjects: @"Coupons", @"Stores", nil];
    UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:itemArray];
    CGFloat size=250;
    segmentedControl.frame = CGRectMake((vc_temp.view.frame.size.width-size)/2, point.y+navBarHeight*2/3, size, navBarHeight*2/3);
    [segmentedControl addTarget:self action:@selector(MySegmentControlAction:) forControlEvents: UIControlEventValueChanged];
        //options
        segmentedControl.selectedSegmentIndex = 0;
        segmentedControl.tintColor = [Utilities colorWithHexString:secondary];
        segmentedControl.layer.cornerRadius = segmentedControl.frame.size.height/2;
        segmentedControl.layer.borderWidth = 0.5f;
        segmentedControl.layer.borderColor = [Utilities colorWithHexString:secondary].CGColor;
        segmentedControl.layer.masksToBounds = YES;
    [myView addSubview:segmentedControl];
    
    //---Custom BACK BUTTON function
    backBarBtnItem = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(onBackClick:)];
    vc_temp.navigationItem.leftBarButtonItem = backBarBtnItem;
    vc_temp.navigationController.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
    
    [self.tabBarController.tabBar setHidden:YES];
    [self pushViewController:vc_temp animated:YES];
    
    
    //------Initializing SCROLLview with text; scrollview gets cleared everytime you start typing
    self.searchDropDown = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.navigationBar.frame.size.height*4, vc_temp.view.frame.size.width, vc_temp.view.frame.size.height)];
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(32, 50, self.searchDropDown.frame.size.width-64,  50)];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Search for stores and exclusive offers by brands and stores around you."];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-SemiBold" size:19] range:NSMakeRange(0, string.length-1)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Medium" size:19] range:NSMakeRange(11, 6)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Medium" size:19] range:NSMakeRange(32, 6)];   //for 'offers' , unlock is at 42th
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:black_two] range:NSMakeRange(0, string.length-1)];
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:secondary] range:NSMakeRange(11,6)];
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:secondary] range:NSMakeRange(32,6)];    //for 'offers'
    title.attributedText = string;
    title.numberOfLines = 0;
    title.adjustsFontSizeToFitWidth = YES;
    title.lineBreakMode = NSLineBreakByTruncatingTail;
    title.contentMode = UIViewContentModeCenter;
    title.textAlignment = NSTextAlignmentCenter;
    
    [self.searchDropDown addSubview:title];
    self.searchDropDown.contentSize = CGSizeMake(self.searchDropDown.frame.size.width, self.searchDropDown.frame.size.height);
    [self.view addSubview:self.searchDropDown ];
}

- (void)searchFieldDidChange:(NSNotification *)notification {
    [self searchForBrandWithText:self.textField.text];
}

- (void)MySegmentControlAction:(UISegmentedControl *)segment
{
    if(segment.selectedSegmentIndex == 0)
        segment_number =0;
    else if(segment.selectedSegmentIndex == 1)
        segment_number =1;
    
    [self searchFieldDidChange:nil];
    return;
}

//------------------------Location autoComplete search
NSString* const type=@"type";
NSString* const stores=@"stores";
NSString* const coupons=@"coupons";
NSString* const label=@"label";
NSString* const subLabel=@"subLabel";
NSString* const subLabel2=@"subLabel2";
NSString* const uniqueID=@"uniqueID";

- (void)searchForBrandWithText:(NSString *)searchString{
#pragma mark SearchMap Autocomplete
    //---Clear the previous dropDown and redraw
    [self.searchDropDown removeFromSuperview];
    if([searchString isEqualToString:@""])
        return;
    self.searchDropDown = [[UIScrollView alloc] initWithFrame:CGRectMake(0, self.navigationBar.frame.size.height*4, self.view.frame.size.width, self.view.frame.size.height)];
    
    NSDictionary *tabledata;
    if(segment_number==0){ //Coupon names
        //-----Store names , in mall , and floor level
        NSString *pre = @"SELECT coupon_info.id,coupon_info.brand_name,coupon_info.name, coupon_info.expiry, zone_info.zone_name";
        NSString *condition = [NSString stringWithFormat:@"INNER JOIN zone_info ON coupon_info.zone_id=zone_info.zone_id WHERE brand_name LIKE '%%%@%%'",searchString];
        tabledata = [[appDelegate DB] displayTable:@"coupon_info" preCondition:pre selectfromTableNamePLUSCondition:condition];
        
        NSDictionary *parameters = @{type: coupons,
                                     label: @"brand_name",
                                     subLabel: @"zone_name",
                                     subLabel2: @"name",
                                     uniqueID: @"id"
                                     };
        [self search_withKeys:tabledata withParameters:parameters];
    }
    else if(segment_number==1){ //Store names
        //-----Store names , in mall , and floor level
        NSString *pre = @"SELECT store_data.store_id, store_data.store_name, zone_info.zone_name, store_data.F_Level";
        NSString *condition = [NSString stringWithFormat:@"INNER JOIN zone_info ON store_data.zone_id=zone_info.zone_id WHERE store_id IN (SELECT store_id FROM tag_map WHERE tags LIKE '%%%@%%') OR store_name LIKE '%%%@%%' ",searchString,searchString];
        tabledata = [[appDelegate DB] displayTable:@"store_data" preCondition:pre selectfromTableNamePLUSCondition:condition];
        
        NSDictionary *parameters = @{type: stores,
                                     label: @"store_name",
                                     subLabel: @"zone_name",
                                     subLabel2: @"F_Level",
                                     uniqueID: @"store_id"
                                     };
        [self search_withKeys:tabledata withParameters:parameters];
    }
}

-(void)search_withKeys:(NSDictionary *)tabledata withParameters:(NSDictionary *)params{
    CGFloat yPos =10;
    //---START LOOP
    for (int i=0; i<[tabledata count]; i++) {
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tabledata valueForKey:key];
        NSString *main_label = [dict valueForKey:params[label]];
        NSString *sub_label = [dict valueForKey:params[subLabel]];
        NSString *sub_label_2 = [dict valueForKey:params[subLabel2]];
        NSString *unique_id = [dict valueForKey:params[uniqueID]];
        //-----Button
        CGFloat fontSize = 17;
        CGFloat gap = 5;    //Leaves gap from button start to where Label will start
        CGRect button_frame = CGRectMake(16, yPos, self.view.frame.size.width-32, fontSize*2+gap*2);   //fontsize*2 is done because of label2
        yPos += button_frame.size.height+gap*2;
        UICustomButton *button = [[UICustomButton alloc] initWithFrame:button_frame];
        button.unique_id = unique_id;
        [button setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"eaeaea"]] forState:UIControlStateHighlighted];
        [button setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"ffffff"]] forState:UIControlStateNormal];
        //---STORE NAME Labels
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, gap , self.view.frame.size.width-32, fontSize+gap)];
        label.text = main_label;
        label.font =[UIFont fontWithName:@"Raleway-Regular" size:fontSize];
        label.textColor = [Utilities colorWithHexString:@"1e1e1e"];
        [button addSubview:label];                      //--addSubView mallName
        //---MALL NAME Label 2
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(0, gap+ fontSize+2.5 , self.view.frame.size.width-32, fontSize)];
        if([params[type] isEqualToString:stores]){
            label2.text = [NSString stringWithFormat:@"%@, Floor no. %@", sub_label, sub_label_2];
            label2.font =[UIFont fontWithName:@"Raleway-Regular" size:label2.frame.size.height-2];
        }
        else{
            button.type = COUPON;
            button.vc = self;
            Utilities *uc_instance = [[Utilities alloc]init];
            [button addTarget:self action:@selector(onSearchCouponClick:) forControlEvents:UIControlEventTouchUpInside];       //+(void)buttonMethod is in Utilities
            label2.text = [NSString stringWithFormat:@"%@, %@", sub_label, sub_label_2];
            label2.font =[UIFont fontWithName:@"Raleway-Regular" size:label2.frame.size.height-3];
        }
        label2.textColor = [Utilities colorWithHexString:@"9e9e9e"];
        [button addSubview:label2];                      //--addSubView mallName
        //-00 DrawLine
        CGRect frame = button.frame;
        CGPoint point = CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
        UIView *Line = [[UIView alloc]initWithFrame:CGRectMake(16, point.y , self.view.frame.size.width -32, 0.5)];
        Line.backgroundColor = [Utilities colorWithHexString:@"dadada"];
        [self.searchDropDown addSubview:Line];            //--addSubView Line
        //-00
        [self.searchDropDown addSubview:button];          //--addSubView BUTTON
        self.searchDropDown.contentSize = CGSizeMake(self.navigationBar.frame.size.width, self.searchDropDown.frame.origin.y+yPos);
    }//@end LOOP
    [self.view addSubview:self.searchDropDown];
}

-(void)onSearchCouponClick:(UICustomButton*)button{
    [self.view endEditing:YES];
    [Utilities couponCardwithID:button.unique_id andViewController:button.vc];
}


//----------------------------------------------------------------
#pragma mark - Others
//----------------------------------------------------------------
//---List of our own malls
-(void)showMallsList:(UIViewController*)vc_temp withScroller:(UIScrollView*)myScrollView andPoint:(CGPoint)locbuttonPoint {
    UILabel *heading = [[UILabel alloc] initWithFrame:CGRectMake(16, locbuttonPoint.y+19*2, vc_temp.view.frame.size.width-32, textField.frame.size.height)];
    heading.text = @"Our Malls in Bangalore";
    heading.font =[UIFont fontWithName:@"Raleway-Bold" size:18];
    heading.textColor = [Utilities colorWithHexString:@"1e1e1e"];
    [myScrollView addSubview:heading]; //--addSubView OurMalls Heading
    //-00 DrawLine
    CGRect frame = heading.frame;
    CGPoint point = CGPointMake(CGRectGetMaxX(frame), CGRectGetMaxY(frame));
    UIView *Line = [[UIView alloc]initWithFrame:CGRectMake(16, point.y + 2, self.view.frame.size.width -32, 2)];
    Line.backgroundColor = [Utilities colorWithHexString:@"dadada"];
    [myScrollView addSubview:Line]; //--addSubView Line
    //-00
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"zone_info" preCondition:nil selectfromTableNamePLUSCondition:@"ORDER BY zone_name ASC"];
    NSString *regionName = nil;
    CGFloat spaceFromHeading = 15;
    CGFloat yPos = (point.y)+spaceFromHeading;
    CGFloat scrollViewContentSize = yPos;
    CGFloat width = vc_temp.view.frame.size.width ;
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        regionName = [dict valueForKey:@"zone_name"];
        NSString *address = [dict valueForKey:@"descp"];
        NSString *zone_id = [dict valueForKey:@"zone_id"];
        if([zone_id isEqualToString:selectZoneID])
            continue;
        //---Button
        CGFloat fontSize = 15;
        CGFloat gap_name_addr= 3;
        CGFloat fontSize_addr = 13;
        CGFloat gap_interMall = 7.5;
        CGRect button_frame = CGRectMake(16, yPos, vc_temp.view.frame.size.width-32, fontSize+fontSize_addr+gap_name_addr+gap_interMall);
        UICustomButton *button = [[UICustomButton alloc] initWithFrame:button_frame];
        button.unique_id = zone_id;
        [button addTarget:self action:@selector(selectRegion:) forControlEvents:UIControlEventTouchUpInside];
        [button setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"eaeaea"]] forState:UIControlStateHighlighted];
        [button setBackgroundImage:[Utilities imageWithColor:[Utilities colorWithHexString:@"ffffff"]] forState:UIControlStateNormal];
            //---Creating Labels
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0 , vc_temp.view.frame.size.width-32, fontSize+gap_name_addr)];
            label.text = regionName;
            label.font =[UIFont fontWithName:@"Raleway-Bold" size:fontSize];
            label.textColor = [Utilities colorWithHexString:@"1e1e1e"];
            [button addSubview:label];         //--addSubView mallName
            //--Creating addr labels
            yPos+=fontSize+gap_interMall;
            scrollViewContentSize += fontSize+gap_interMall;//SCROLLVIEW
            UILabel *addr_label = [[UILabel alloc]initWithFrame:CGRectMake(0, fontSize+gap_interMall, vc_temp.view.frame.size.width-32, fontSize_addr+3)];
            [addr_label setFont:[UIFont fontWithName:@"Raleway-Light" size:fontSize_addr]];
            [addr_label setText:address];
            [addr_label setTextColor:[Utilities colorWithHexString:@"1e1e1e"]];
            addr_label.textAlignment = UITextAlignmentLeft;
            [button addSubview:addr_label];  //---addSubview address
        [myScrollView addSubview:button];           //--addSubView BUTTON
        //-00 DrawLine
        yPos += fontSize_addr+4.5;
        scrollViewContentSize += fontSize_addr+4.5;//SCROLLVIEW
        UIView *Line2 = [[UIView alloc]initWithFrame:CGRectMake(16, scrollViewContentSize, self.view.frame.size.width -32, 1)];
        Line2.backgroundColor = [Utilities colorWithHexString:@"cecece"];
        [myScrollView addSubview:Line2];            //--addSubView Line
        //-00
        yPos += 5;
        scrollViewContentSize += 5;//SCROLLVIEW
        myScrollView.contentSize = CGSizeMake(width, scrollViewContentSize+25);
    }//@end ForLoop
}

-(void)selectRegion:(UICustomButton *)button {
    [Utilities setRegionSharedPreference:button.unique_id];
    [self popToRootViewControllerAnimated:YES];
}

//---
// BUTTON for each autocomplete suggestion in LOCATION SEARCH
//---
-(void)setLocationAutocompleteButton:(UICustomButton *)button {
    [self.placesClient lookUpPlaceID:button.unique_id callback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            NSLog(@"Place Details error %@", [error localizedDescription]);
            return;
        }
        if (place != nil) {
            NSLog(@"Place name %@", place.name);
            NSLog(@"Place address %@", place.formattedAddress);
            NSLog(@"Place placeID %@", place.placeID);
            NSLog(@"Place attributions %@", place.attributions);
            [ViewController getClosestZoneLocation_ZoneID:place.coordinate.latitude :place.coordinate.longitude];   //-->Here ViewController is class Name of HomeController
        } else {
            NSLog(@"No place details for %@", button.unique_id);
        }
        [self popToRootViewControllerAnimated:YES];
    }];
}

- (void)onBackClick:(id)sender {
    NSLog(@"onBackClick called");
#pragma mark REmove searchField dropdown
    [self.searchDropDown removeFromSuperview];
    [self popViewControllerAnimated:YES];
}


//-------------------------------------------------------
#pragma mark - searchField Delegates
//-------------------------------------------------------
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
//When user touches outside
//Ensure that the user Interaction of the scroll Views and other views are enabled for this to work
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

-(void)textFieldDidEndEditing:(NSNotification *)notification {
#pragma mark Delete table view dropdown
    [_dropDown removeFromSuperview];
}


@end
