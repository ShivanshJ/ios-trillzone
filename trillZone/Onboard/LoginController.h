//
//  LoginController.h
//  trillZone
//
//  Created by Shivansh on 11/7/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "AppDelegate.h"
#import "Utilities.h"
#import "HomeController.h"

@interface LoginController : UIViewController <GIDSignInUIDelegate, FBSDKLoginButtonDelegate>

@property (nonatomic, strong) AppDelegate *appDelegate; //Global array declaration

@property(weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

- (IBAction)guestButton:(id)sender;

- (IBAction)FBbutton:(id)sender;
- (IBAction)Gbutton:(id)sender;

+(void) WritingSharedPreferences : (NSDictionary*) jsondata;
+(BOOL) check_SharedPreferences;

//--For onboarding purposes only
@property NSUInteger pageIndex;

@end
