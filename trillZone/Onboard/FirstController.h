//
//  FirstController.h
//  trillZone
//
//  Created by Shivansh on 1/17/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface FirstController : UIViewController <UIPageViewControllerDataSource>

//----Onboarding screen
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (strong, nonatomic) NSArray *pageTitles;
@property (strong, nonatomic) NSArray *pageImages;

@end
