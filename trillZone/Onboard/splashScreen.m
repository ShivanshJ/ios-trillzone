//
//  splashScreen.m
//  trillZone
//
//  Created by Mac Mini on 6/5/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "splashScreen.h"
#import <Lottie/Lottie.h>


@interface splashScreen ()

@end

@implementation splashScreen

@synthesize label;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSString *myFilePath =  [[NSBundle mainBundle] pathForResource:@"search_locate" ofType:@"json"];
    NSData *myData = [NSData dataWithContentsOfFile:myFilePath];
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:myData
                                                             options:kNilOptions
                                                               error:&error];
    LOTAnimationView *aview = [LOTAnimationView animationFromJSON:jsonDict];
    CGFloat sizeOfAnim = 220;
    aview.frame = CGRectMake( (self.view.frame.size.width-sizeOfAnim)/2, (self.view.frame.size.height)*0.3, sizeOfAnim , sizeOfAnim);
    //aview.center = self.view.center;
    aview.contentMode = UIViewContentModeScaleAspectFit;
    aview.loopAnimation = YES;
    [self.view addSubview:aview];
    [aview play];
    
    //------Text Animation
    self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, aview.frame.origin.y+aview.frame.size.height-10, self.view.frame.size.width, 30)];
    self.label.text = @"SEARCH";
    self.label.textColor = [Utilities colorWithHexString:black_one];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont fontWithName:@"Raleway-SemiBold" size:14];
    [NSTimer scheduledTimerWithTimeInterval:2.28f target:self selector:@selector(fireEvent) userInfo:nil repeats:YES];
    [self.view addSubview:self.label];
    
    
    CGFloat height=30;
    //lABEL TRILLBIT
    UILabel *heading = [[UILabel alloc]init];
    heading.text = @"TRILLBIT";
    CGFloat newWidth = self.view.frame.size.width;//heading.intrinsicContentSize.width;
    heading.frame = CGRectMake(0,  self.view.frame.size.height - height*3.0, newWidth, height);
    heading.backgroundColor = [UIColor whiteColor];
    heading.textColor = [Utilities colorWithHexString:secondary];
    heading.textAlignment = NSTextAlignmentCenter;
    heading.font = [UIFont fontWithName:@"Raleway-Bold" size:28];
    [self.view addSubview:heading];
}

int iteration = -1;
-(void)fireEvent
{   iteration++;
    
    NSArray *myArray = @[@"SOUND",@"LOCATION"];
    //NSLog(@"Splash screen.m : Iteration %@ %d", [myArray objectAtIndex:iteration], iteration);
    
    //FadeOut
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.2];
    //[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    self.label.alpha = 0;
    [UIView commitAnimations];
    self.label.text = [myArray objectAtIndex:iteration];
    //FADEin
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.2];
    //[label setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
    self.label.alpha = 1;
    [UIView commitAnimations];
    
    if (iteration>=myArray.count-1)
        iteration=-1;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
