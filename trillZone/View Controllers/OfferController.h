//
//  OfferController.h
//  trillZone
//
//  Created by Shivansh on 2/2/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Utilities.h"
#import "CouponView.h"


@interface OfferController : UIViewController

@property (nonatomic, strong) AppDelegate *appDelegate;
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;

-(void)allCouponViewController: (id)sender;

@end
