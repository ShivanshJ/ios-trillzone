//
//  ViewController.h
//  trillZone
//
//  Created by Shivansh on 10/27/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Utilities.h"
#import "DragUpViewController.h"

#import <trillSDK/trillSDK.h>
#import <CoreLocation/CoreLocation.h>

#import <TOMSMorphingLabel.h>
#import <iCarousel.h>
#import <GradientProgress/GradientProgress-umbrella.h>

#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface ViewController : UIViewController <UIAlertViewDelegate, ISHPullUpContentDelegate>{
    NSString *API_DATABASE_INFO;
    NSString *API_LOGIN_REQUEST;
    NSString *API_VERIFY_OTP;
    NSString *API_COUPON_INFO;
    NSString *API_ZONES_INFO;
    NSString *API_SONGS_LIST;
    NSString *API_USER_ACTIONS;
    NSString *API_BOOKMARK_COUPON;
    
    BOOL current_zone;  //FOR one mall, simple Yes or no.
    NSString *adId; //Advertising Identifier
}

@property (nonatomic, strong) CLLocationManager *locationManager;   //Location,
@property (nonatomic, strong) AppDelegate *appDelegate; //Global array declaration
@property (strong, nonatomic) UIScrollView *myScrollView;

@property (strong,nonatomic) TOMSMorphingLabel *label;
@property (strong,nonatomic) CAShapeLayer *rippleLayer;

//-----ProgressBar
@property (strong, nonatomic)  UIProgressView *progressView;
@property (strong, nonatomic) UILabel *labelProgress;
@property (retain, nonatomic) NSTimer *myTimer;
-(void)progressBarCreation;
-(void)setCurrentTime:(int)minutes :(int)seconds;

+ (BOOL)getClosestZoneLocation_ZoneID : (double)startLatitude : (double)startLongitude; //---Function to set to closest location (For navBar)


@property (weak,nonatomic) UIView *rootView;

@property (strong, nonatomic) UIImageView *hotTile;
@property (strong, nonatomic) AVPlayer *player;
@property (strong, nonatomic) AVPlayerViewController *controller;





@end

