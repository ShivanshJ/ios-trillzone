//
//  BottomViewController.m
//  trillZone
//
//  Created by Mac Mini on 6/1/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "BottomViewController.h"

@interface BottomViewController ()

@end

@implementation BottomViewController

@synthesize appDelegate;
@synthesize brand_title, textView, image_campaign, descriptions, label, mycarousel;
@synthesize roundedView, handleView,topView,topLabel, myScrollView;

CGFloat yGAP,scrollStart ;
- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    self.roundedView = [[ISHPullUpRoundedView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:self.roundedView];
    //-----------pullUp
    yGAP = 50;
    scrollStart = 45;
    self.topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, yGAP)];
    self.topLabel =[[UILabel alloc]initWithFrame:CGRectMake(0, 16, self.view.frame.size.width, yGAP-20)];
    self.topLabel.textAlignment = NSTextAlignmentCenter;
    self.handleView = [[ISHPullUpHandleView alloc]initWithFrame:CGRectMake((self.view.frame.size.width-yGAP)/2, 5, yGAP, 10)];
    [self.topView addSubview:self.handleView];
    [self.topView addSubview:self.topLabel];
    self.myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, yGAP+scrollStart, self.view.frame.size.width, self.view.frame.size.height-(yGAP+scrollStart))];
    [self.roundedView addSubview:self.topView];     //-----addsubview
    [self.roundedView addSubview:self.myScrollView];  //-----addsubview
    //TapGesture
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]init];
    [tap addTarget:self action:@selector(handleTapGesture:)];
    [self.topView addGestureRecognizer:tap];
    
    //------HOT offer label
    UILabel *header = [[UILabel alloc]initWithFrame:CGRectMake(0, yGAP+20, self.view.frame.size.width, 20)];
    header.text = @"Hot Offers";
    header.textColor = [Utilities colorWithHexString:black_two];
    header.textAlignment = NSTextAlignmentCenter;
    header.font = [UIFont fontWithName:@"Raleway-SemiBold" size:20];
    [self.roundedView addSubview:header];
    
    //------Carousel
    self.mycarousel.tag = 1;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    firstAppearanceCompleted = true;
    //----Reset everything
    [self.image_campaign removeAllObjects];
    [self.brand_title removeAllObjects];
    [self.descriptions removeAllObjects];
    [self.label removeFromSuperview];
    [self.textView removeFromSuperview];
    
    //----Retrieval from Database
    NSString *region = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
    NSString *condition = [NSString stringWithFormat:@"WHERE LOWER(type)=LOWER('hot') AND zone_id='%@'",region];
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
    self.brand_title = [[NSMutableArray alloc]init];
    self.image_campaign = [[NSMutableArray alloc]init];
    self.descriptions = [[NSMutableArray alloc]init];;
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        [self.brand_title addObject:[dict valueForKey:@"brand_name"]];
        [self.descriptions addObject:[dict valueForKey:@"descp"]];
        [self.image_campaign addObject:[dict valueForKey:@"image"]];
    }
    NSLog(@"BottomViewController.m/viewDidAppear: %@",self.descriptions);
    [self loadCarousel];
}


-(void)loadCarousel {
    [[self.view viewWithTag:1] removeFromSuperview];
    
    self.mycarousel = [[iCarousel alloc]initWithFrame:CGRectMake(0, 0 , self.view.frame.size.width, 200)];
    self.mycarousel.tag = 1;
    self.mycarousel.dataSource = self;
    self.mycarousel.delegate = self;
    self.mycarousel.type = iCarouselTypeRotary;
    self.mycarousel.pagingEnabled = YES;
    [self.myScrollView addSubview:self.mycarousel];     //-----addSubView mycarousel
    [self.mycarousel reloadData];
    
    //----Brand label & description
    //It is initialized in -(void)properties_textView_label_addSubview but used in carousel delegate
   
    contentScrollSize = self.myScrollView.contentSize.height;
}

-(void)properties_textView_label_addSubview{
    //---AddSubView
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, self.mycarousel.frame.size.height+20, self.view.frame.size.width, 20)];
    self.label.textColor = [Utilities colorWithHexString:secondary];
    self.label.textAlignment = NSTextAlignmentCenter;
    self.label.font = [UIFont fontWithName:@"Raleway-Bold" size:18];
    [self.myScrollView addSubview:self.label];      //---addsubview
    
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(20, self.label.frame.origin.y+self.label.frame.size.height, self.view.frame.size.width-40, 200)];
    self.textView.textColor = [Utilities colorWithHexString:@"cccccc"];
    self.textView.textAlignment = NSTextAlignmentCenter;
    self.textView.font = [UIFont fontWithName:@"Raleway-Medium" size:15];
    //*****Description Dynamic SIZE
    self.textView.backgroundColor = [UIColor clearColor];
    // get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth = self.textView.frame.size.width;
    CGSize newSize = [self.textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = self.textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    self.textView.frame = newFrame;
    self.textView.userInteractionEnabled = NO;
    [self.myScrollView addSubview:self.textView];      //---addsubview
    
    self.myScrollView.contentSize = CGSizeMake(self.myScrollView.frame.size.width, 200+20+20+newFrame.size.height);
    
    self.textView.clearsContextBeforeDrawing =YES;
    self.label.clearsContextBeforeDrawing = YES;
}

//------------------------------------------------
#pragma mark - Delegates for Carousel

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    CGFloat result;
    switch(option) {
        case iCarouselOptionSpacing:
            result = 1.1 ; // If the width of your items is 40 e.g, the spacing would be 4 px.
            break;
        default:
            result = value;
            break;
    }
    return result;
}

-(UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view{
    //create new view if no view is available for recycling
        if (view == nil)
        {
            //don’t do anything specific to the index within
            //this `if (view == nil) {…}` statement because the view will be
            //recycled and used with other index values later
            
            view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width*0.5, self.view.frame.size.width*0.5)];
            [((UIImageView *)view) sd_setImageWithURL:[NSURL URLWithString:[self.image_campaign objectAtIndex:index]] placeholderImage:[UIImage imageNamed:@"back_no_trill"] options:SDWebImageRefreshCached];
            
            //((UIImageView *)view).image = [UIImage imageNamed:[self.image_campaign objectAtIndex:index]];
            view.contentMode = UIViewContentModeScaleAspectFill;
            view.clipsToBounds = YES;
        }
        else
        {
            //get a reference to the label in the recycled view
            self.label = (UILabel *)[self.view viewWithTag:1];
        }
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {…}` check otherwise
    //you’ll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    //self.label.text = [self.brand_title objectAtIndex:index];

    //self.myScrollView.contentSize = (sel)
    //--Boundary Outline
    view.layer.borderWidth = 3.0;
    view.layer.borderColor = [[Utilities colorWithHexString:black_two] CGColor];
    return view;
}

CGFloat contentScrollSize = 0;
- (void)carouselDidEndScrollingAnimation:(iCarousel *)aCarousel
{
    //First remove the views for non - overlap
    [self.textView removeFromSuperview];
    [self.label removeFromSuperview];
    [self properties_textView_label_addSubview];
    
    [self.label setText:[NSString stringWithFormat:@"%@", [self.brand_title objectAtIndex:aCarousel.currentItemIndex]]];
    [self.textView setText:[NSString stringWithFormat:@"%@", [self.descriptions objectAtIndex:aCarousel.currentItemIndex]]];
    
    //*****Description Dynamic SIZE
    CGFloat fixedWidth = self.textView.frame.size.width;
    CGSize newSize = [self.textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = self.textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    self.textView.frame = newFrame;

    self.myScrollView.contentSize = CGSizeMake(self.myScrollView.frame.size.width, contentScrollSize+ newFrame.size.height);
    NSLog(@"%@",[self.descriptions objectAtIndex:aCarousel.currentItemIndex]);
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index
{
    //create a numbered view
//    UIImageView *image_view = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 200, 200)];
//    image_view.contentMode = UIViewContentModeScaleAspectFit;
//    image_view.image = [UIImage imageNamed:[self.image_campaign objectAtIndex:index]];

    UIView *view = [[UIImageView alloc] initWithImage:[UIImage imageNamed:[self.image_campaign objectAtIndex:index]]];
    return view;
}

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    NSLog(@"numberOfItemsInCarousel: ",[self.image_campaign count]);
    return [self.image_campaign count];
}

- (NSUInteger)numberOfVisibleItemsInCarousel:(iCarousel *)carousel
{
    //limit the number of items views loaded concurrently (for performance reasons)
    return [self.image_campaign count];
}

- (NSUInteger)numberOfPlaceholdersInCarousel:(iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 0;
}


- (CGFloat)carouselItemWidth:(iCarousel *)carousel
{
    //usually this should be slightly wider than the item views
    return 240;
}


- (BOOL)carouselShouldWrap:(iCarousel *)carousel
{
    //wrap all carousels
    return _wrap;
}


//-------------------------------------------------------
#pragma mark - Delegates for PullUp
//-------------------------------------------------------
CGFloat halfWayPoint = 0;
BOOL firstAppearanceCompleted = false;

-(void)handleTapGesture:(UITapGestureRecognizer*)gesture {
    NSLog(@"Entered tapGesture");
    if([self.pullUpController isLocked])
        return;
    
    NSLog(@"tapGesture not locked");
    [self.pullUpController toggleStateAnimated:YES];
}

//MARK: Max height PULLUP
-(CGFloat)pullUpViewController:(ISHPullUpViewController *)pullUpViewController maximumHeightForBottomViewController:(UIViewController *)bottomVC maximumAvailableHeight:(CGFloat)maximumAvailableHeight
{
    CGFloat totalHeight = self.view.frame.size.height;
    halfWayPoint = totalHeight/2.0;
    return totalHeight;
}

//MARK: Minimum Height PULLUP
-(CGFloat)pullUpViewController:(ISHPullUpViewController *)pullUpViewController minimumHeightForBottomViewController:(UIViewController *)bottomVC {
    return [self.topView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
}


//MARK: Target Height
-(CGFloat)pullUpViewController:(ISHPullUpViewController *)pullUpViewController targetHeightForBottomViewController:(UIViewController *)bottomVC fromCurrentHeight:(CGFloat)height{
    // if around 30pt of the half way point -> snap to it
    int x = halfWayPoint - height;
    //int y = ((x<0) ?  -x : x );
    if( x > 0) {
        return 0;
    }
    else if (x<=0){
        return self.view.frame.size.height;
    }
    // default behaviour
    return height;
}


-(void)pullUpViewController:(ISHPullUpViewController *)pullUpViewController updateEdgeInsets:(UIEdgeInsets)edgeInsets forBottomViewController:(UIViewController *)contentVC{
    self.myScrollView.contentInset = edgeInsets;
}




-(void)pullUpViewController:(ISHPullUpViewController *)pullUpViewController didChangeToState:(ISHPullUpState)state{
    self.topLabel.text = [self textForState:state];
    self.topLabel.textColor = [Utilities colorWithHexString:@"a3a3a3"];
    self.topLabel.font = [UIFont fontWithName:@"Raleway-Regular" size:16];
    
    [self.handleView setState:[ISHPullUpHandleView handleStateForPullUpState:state] animated:firstAppearanceCompleted];
    
    [UIView animateWithDuration:0.4 animations:^{
        self.view.alpha = (state==ISHPullUpStateCollapsed)?0.4:1;
    }];
}

-(NSString*)textForState:(ISHPullUpState)state{
    switch(state){
        case ISHPullUpStateCollapsed: return @"Drag Up or Tap";
        case ISHPullUpStateIntermediate: return @"Drag";
        case ISHPullUpStateDragging: return @"";
        case ISHPullUpStateExpanded: return @"Drag down or tap";
    }
    return @"Drag down or tap";
}
@end
