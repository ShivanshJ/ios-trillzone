//
//  DragUpViewController.m
//  trillZone
//
//  Created by Mac Mini on 5/31/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "DragUpViewController.h"

@interface DragUpViewController ()

@end

@implementation DragUpViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    
    ViewController *HomeView = [[ViewController alloc]init];//1
    BottomViewController *bottomview = [[BottomViewController alloc] init];//2

    self.contentViewController = HomeView;
    self.bottomViewController = bottomview;
    bottomview.pullUpController = self;
    
    self.contentDelegate = HomeView;
    self.sizingDelegate = bottomview;
    self.stateDelegate = bottomview;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
