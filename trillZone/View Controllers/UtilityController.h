//
//  UtilityController.h
//  trillZone
//
//  Created by Shivansh on 2/2/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"

@interface UtilityController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *myScrollView;
@end
