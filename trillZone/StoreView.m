//
//  StoreView.m
//  trillZone
//
//  Created by Shivansh on 2/9/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "StoreView.h"

@interface StoreView ()
{
    CGFloat leaveTopHeight;
}
@end

@implementation StoreView

@synthesize appDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController.tabBar setHidden:YES];
    self.navigationController.navigationBar.barTintColor = [Utilities colorWithHexString:black_one];
    self.navigationController.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
    
    leaveTopHeight =  0;//16+self.navigationController.navigationBar.frame.size.height +
    
    //---Colour of Navigation Bar
//    UIView *solidcolor = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, leaveTopHeight)];
//    solidcolor.backgroundColor = [Utilities colorWithHexString:primary];
//    [self.view addSubview:solidcolor];
    
    //-----------CAPSPageMenu
    // Array to keep track of controllers in page menu
    NSMutableArray *controllerArray = [NSMutableArray array];
    // Create variables for all view controllers you want to put in the
    // page menu, initialize them, and add each to the controller array.
    // (Can be any UIViewController subclass)
    // Make sure the title property of all view controllers is set
    // Example:
    NSString *regionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
    NSString *condition = [NSString stringWithFormat:@"WHERE zone_id='%@' ORDER BY F_Level ASC",regionID];
    NSDictionary *tableData = [[appDelegate DB]displayTable:@"store_data" preCondition:@"SELECT DISTINCT F_Level,store_location" selectfromTableNamePLUSCondition:condition];
    
    if ([tableData count]==0){
        //-----Text HOT OFFERS
        [Utilities showAlertWithTitle:@"Stores N/A" andMessage:@"Store data for this mall is being updated"];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        NSString *levels = [dict valueForKey:@"F_Level"];
        NSString *level_name = [dict valueForKey:@"store_location"];
        UIViewController *vc = [[UIViewController alloc]init];
        [self customView:vc :levels];
        vc.title = level_name;
        [controllerArray addObject:vc];
    }
    
    // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
    // Example:
    //----nOTE: Set segmented control as NO to get desired effect of lengthiness, i.e, wrapping font as horizontal scrolling
    NSDictionary *parameters = @{CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(NO),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 CAPSPageMenuOptionMenuItemWidth: @(120),
                                 
                                 CAPSPageMenuOptionViewBackgroundColor: [Utilities colorWithHexString:black_one],
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [Utilities colorWithHexString:black_one],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [Utilities colorWithHexString:secondary],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [Utilities colorWithHexString:secondary],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"Raleway-Bold" size:12],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [Utilities colorWithHexString:secondary]
                                 };
    // Initialize page menu with controller array, frame, and optional parameters
    self.pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, leaveTopHeight, self.view.frame.size.width, self.view.frame.size.height - leaveTopHeight) options:parameters];
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.view addSubview:_pageMenu.view];
    ///-----------CAPSPageMenu @end
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.isMovingFromParentViewController) {
        [self.tabBarController.tabBar setHidden:NO];
    }
}


-(void)customView: (UIViewController *)vc :(NSString *)levels{
    //---ScrollView
    UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height)];
    [vc.view addSubview:myScrollView];
    
    vc.view.backgroundColor = [UIColor whiteColor];
    
    //----For UIImages in scrollView
   
    CGFloat Titleopacity = 1.0f;
    CGFloat yPOS = 16 ;
    CGFloat scrollViewContentSize = yPOS + leaveTopHeight + 100;//+ 50 for height of the TabMenuBar
    
    //---Retrieval from Database
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"store_data" preCondition:nil selectfromTableNamePLUSCondition:[NSString stringWithFormat:@"WHERE F_Level='%@'",levels] ];
    NSMutableArray *unique_id = [[NSMutableArray alloc] init];
    NSMutableArray *titles = [[NSMutableArray alloc] init];
    NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        [unique_id addObject:[dict valueForKey:@"store_id"]];
        [titles addObject:@""];
        [image_URLs addObject:[dict valueForKey:@"store_image"]];
    }
    
    NSDictionary *parameters = @{BOX_type: NULLVOID,
                                 BOX_id: unique_id,
                                     
                                 BOX_titles: titles,
                                 BOX_imageURL: image_URLs,
                                 BOX_titleOpacity: @(Titleopacity),
                                 BOX_yPOS: @(yPOS),
                                 BOX_scrollViewContentSize:@(scrollViewContentSize)
                                 };
    
    [Utilities makeSquareBoxes:parameters withScrollView:myScrollView andViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
