//
//  CouponView.m
//  trillZone
//
//  Created by Shivansh on 2/2/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "CouponView.h"


@interface CouponView ()
{
    CGFloat leaveTopHeight;
}
@end

@implementation CouponView

@synthesize appDelegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self.tabBarController.tabBar setHidden:YES];
    //----EEnabling navigation bar colour (when translucent = YES)
    [[self navigationController] navigationBar].barTintColor = [Utilities colorWithHexString:black_one];
    self.navigationController.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
    
    leaveTopHeight = 0;//self.navigationController.navigationBar.frame.size.height*3/2;
    
//    UIView *solidcolor = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, leaveTopHeight)];
//    solidcolor.backgroundColor = [Utilities colorWithHexString:primary];
//    [self.view addSubview:solidcolor];
    
    //-----------CAPSPageMenu
    // Array to keep track of controllers in page menu
    NSMutableArray *controllerArray = [NSMutableArray array];
    // Create variables for all view controllers you want to put in the
    // page menu, initialize them, and add each to the controller array.
    // (Can be any UIViewController subclass)
    // Make sure the title property of all view controllers is set
    // Example:
//    UIViewController *controller = [[UIViewController alloc] initWithNibName:@"LoginID" bundle:nil];
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    UIViewController *vc = [[UIViewController alloc]init];
    [self customView:vc IS_FAV:NO];
    UIViewController *vc2 = [[UIViewController alloc]init];
    [self customViewCATEGORY:vc2];
    vc.title = @"ALL";
    vc2.title = @"CATEGORY";
    [controllerArray addObject:vc];
    [controllerArray addObject:vc2];
    
    // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
    // Example:
    //----nOTE: Set segmented control as NO to get desired effect of lengthiness, i.e, wrapping font as horizontal scrolling
    NSDictionary *parameters = @{CAPSPageMenuOptionMenuItemSeparatorWidth: @(4.3),
                                 CAPSPageMenuOptionUseMenuLikeSegmentedControl: @(YES),
                                 CAPSPageMenuOptionMenuItemSeparatorPercentageHeight: @(0.1),
                                 
                                 CAPSPageMenuOptionViewBackgroundColor: [Utilities colorWithHexString:black_one],
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [Utilities colorWithHexString:black_one],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [Utilities colorWithHexString:secondary],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [Utilities colorWithHexString:secondary],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"Raleway-Bold" size:16],
                                 CAPSPageMenuOptionSelectedMenuItemLabelColor: [Utilities colorWithHexString:secondary]
                                 };
    // Initialize page menu with controller array, frame, and optional parameters
    self.pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0, leaveTopHeight, self.view.frame.size.width, self.view.frame.size.height - leaveTopHeight) options:parameters];
    // Lastly add page menu as subview of base view controller view
    // or use pageMenu controller in you view hierachy as desired
    [self.view addSubview:_pageMenu.view];
    ///-----------CAPSPageMenu @end
}


-(void)closeView:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.tabBarController.tabBar setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.isMovingFromParentViewController) {
       [self.tabBarController.tabBar setHidden:NO];
    }
}


//------------------------------
#pragma mark - ViewController Setting
//------------------------------

-(void)customView: (UIViewController *)vc IS_FAV:(BOOL)isFAV{
    //---ScrollView
    UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height)];
    [vc.view addSubview:myScrollView];
    
    //vc.view.backgroundColor = [Utilities colorWithHexString:black_one];
    //----For UIImages in scrollView
    NSArray *myTitles = @[@"1",@"2",@"3",@"4"];
    CGFloat Titleopacity = 1.0f;
    NSArray *myImages = @[@"back_no_trill",@"back_no_trill",@"back_no_trill",@"back_no_trill"];
    CGFloat imageWidth = self.view.frame.size.width - 32;
    CGFloat imageHeight = self.view.frame.size.height*1/4;
    CGFloat yPOS = 30 ;
    CGFloat scrollViewContentSize = yPOS + leaveTopHeight + 100;//+ 50 for height of the TabMenuBar
    //---Retrieval from Database
    NSDictionary *tableData;
    //---Retrieve Data
    if(isFAV == NO){
        NSString *region = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
        NSString *condition = [NSString stringWithFormat:@"WHERE zone_id='%@' ORDER BY name ASC",region];
        tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
    }
    else
        tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:@"WHERE is_fav=1 ORDER BY name ASC"];
    NSMutableArray *unique_id = [[NSMutableArray alloc] init];
    NSMutableArray *titles = [[NSMutableArray alloc] init];
    NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        [unique_id addObject:[dict valueForKey:@"id"]];
        [titles addObject:[dict valueForKey:@"brand_name"]];
        [image_URLs addObject:[dict valueForKey:@"image"]];
    }
    NSDictionary *parameters = @{BOX_type: COUPON,
                                 BOX_id: unique_id,
                                 
                                 BOX_titles: titles,
                                 BOX_imageURL: image_URLs,
                                 BOX_titleOpacity: @(Titleopacity),
                                 BOX_images: myImages,
                                 BOX_imageWidth: @(imageWidth),
                                 BOX_imageHeight: @(imageHeight),
                                 BOX_yPOS: @(yPOS),
                                 BOX_scrollViewContentSize:@(scrollViewContentSize)
                                 };
    [Utilities makeSquareBoxes:parameters withScrollView:myScrollView andViewController:self];
}


-(void)customViewCATEGORY:(UIViewController*)vc{
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"category_info" preCondition:nil selectfromTableNamePLUSCondition:@"ORDER BY category_name ASC"];
    //---ScrollView
    UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height)];
    [vc.view addSubview:myScrollView];
    //-------------Making Rectangles
    CGFloat Titleopacity = 1.0;
    CGFloat imageHeight = self.view.frame.size.height/5;
    //---Retrieval from Database
    NSMutableArray *unique_id = [[NSMutableArray alloc] init];
    NSMutableArray *titles = [[NSMutableArray alloc] init];
    NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        [unique_id addObject:[dict valueForKey:@"category_id"]];
        [titles addObject:[dict valueForKey:@"category_name"]];
        [image_URLs addObject:[dict valueForKey:@"category_image"]];
    }
    CGFloat yPOS = 30 ;
    CGFloat scrollViewContentSize = yPOS*2 + leaveTopHeight + 100; //for height of the TabMenuBar
    
    NSDictionary *parameters = @{   BOX_id: unique_id,
                                    BOX_type: CATEGORY,
                                    
                                    BOX_titles: titles,
                                    BOX_imageURL: image_URLs,
                                    BOX_titleOpacity: @(Titleopacity),
                                    BOX_imageHeight: @(imageHeight),
                                    BOX_yPOS: @(yPOS),
                                    BOX_scrollViewContentSize:@(scrollViewContentSize)
                                    };
    [Utilities makeRectangleBoxes:parameters withScrollView:myScrollView andViewController:self];
}

@end

