//
//  ImageFetcher.m
//  trillZone
//
//  Created by Shivansh on 2/7/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "ImageFetcher.h"


@implementation ImageFetcher

+ (void)prefetchImages:(NSArray *)images
{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    SDWebImagePrefetcher *prefetcher = [SDWebImagePrefetcher sharedImagePrefetcher];
    NSMutableArray *urlsToDownload = [NSMutableArray array];
    for (NSString *img in images) {
        NSURL *imageUrlToDownload = [NSURL URLWithString:img];
        
        __block BOOL flag_continue = NO;
        [manager cachedImageExistsForURL:imageUrlToDownload completion:^(BOOL isInCache) {
            if(isInCache == YES)
                flag_continue = YES ;
            return ;
        }];
            
        if(flag_continue == YES) continue;
        
        
        if (imageUrlToDownload != nil){
            [urlsToDownload addObject:imageUrlToDownload];
        }
    }
    if (urlsToDownload.count != 0) {
        [prefetcher prefetchURLs:urlsToDownload];
    }
}


@end
