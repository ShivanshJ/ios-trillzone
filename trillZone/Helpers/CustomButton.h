//
//  CustomButton.h
//  trillZone
//
//  Created by Shivansh on 2/7/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface UICustomButton : UIButton
{
    NSString *_type;
    NSString *_unique_id;
}


//--For tapping on Categories/Coupons/Utilities etc
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *unique_id;
@property (nonatomic, retain) UIViewController *vc;
//---For close button on CardPopOver
@property (nonatomic, retain) UIView *TOPview;
//---For favorite button on CardPopOver
@property (nonatomic, retain) NSNumber *tag;
@property (nonatomic, retain) UIImageView *imageView;
//---For Exclusive button
@property (nonatomic, retain) NSArray *array;

- (id)initWithFrame:(CGRect)frame uniqueID:(NSString *)unique_id andType:(NSString *)type;
- (id)initCloseButtonWithFrame:(CGRect)frame andView:(UIView *)TOPview;
- (id)initFavButtonWithFrame:(CGRect)frame uniqueID:(NSString *)unique_id andImageView:(UIImageView *)imgview;

- (void)buttonPressed;

@end
