//
//  Utilities.m
//  trillZone
//
//  Created by Shivansh on 1/10/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import "Utilities.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#include "ImageFetcher.h"

#import "OfferController.h"

UITextField *textField;
@implementation Utilities


+ (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

+ (void) postNotification:(NSString *)title withMessage:(NSString *)message{
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate date];
    NSTimeZone* timezone = [NSTimeZone defaultTimeZone];
    notification.timeZone = timezone;
    notification.alertBody = title;
    notification.alertAction = message;
    notification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}

+(void)postGeofenceNotifWithMessage:(NSString*)aMessage andRegionID:(NSString*)aRegionID
{
    UIApplication *app                = [UIApplication sharedApplication];
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    if (notification == nil){return;}
    
    notification.alertBody = @"You've entered a geofence";
    notification.alertAction = @"Open Webview";
    NSDictionary *infoDict = [NSDictionary dictionaryWithObject:aRegionID forKey:aRegionID];
    notification.userInfo = infoDict;
    notification.soundName = UILocalNotificationDefaultSoundName;
    notification.applicationIconBadgeNumber = 1;
    
    [app presentLocalNotificationNow:notification];
}

//-----------------------------------------------
#pragma mark - ui/ux
//-----------------------------------------------
+(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    if ([cString length] != 6) return  [UIColor grayColor];
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

+ (CAShapeLayer *)drawLineWithColour:(NSString*)color andLineWidth:(CGFloat)thickness{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(10.0, 10.0)];
    [path addLineToPoint:CGPointMake(100.0, 100.0)];
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.path = [path CGPath];
    //    shapeLayer.strokeColor = [[UIColor blueColor] CGColor];
    shapeLayer.lineWidth = thickness;
    shapeLayer.fillColor = [Utilities colorWithHexString:color].CGColor;
    
    return shapeLayer;
}

+(void)colorDrawViewGradientLeft:(NSString *)leftColor Right:(NSString *)rightColor andSelfDotView:(UIView *)view{
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    UIColor *leftC= [self colorWithHexString:leftColor];
//    UIColor *middleC= [self colorWithHexString:midColor];
    UIColor *rightC= [self colorWithHexString:rightColor];
    //(0.0, 0.0) is the top left corner while (1.0, 1.0) is the bottom right corner
    theViewGradient.startPoint = CGPointMake(0.0, 0.5);
    theViewGradient.endPoint = CGPointMake(1.0, 0.5);
    theViewGradient.colors = [NSArray arrayWithObjects: (id)leftC.CGColor,(id)rightC.CGColor, nil];
    theViewGradient.frame = view.bounds;
    
    view.layer.shadowRadius  = 1.0f;
    view.layer.shadowColor   = [[UIColor blackColor] CGColor];
    view.layer.shadowOffset  = CGSizeMake(1.5f, 3.0f);
    view.layer.shadowOpacity = 0.9f;
    view.layer.masksToBounds = NO;
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, -1.5f, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(view.bounds, shadowInsets)];
    view.layer.shadowPath    = shadowPath.CGPath;
    
    //Add gradient to view
    [view.layer insertSublayer:theViewGradient atIndex:0];
}

+(void)colorDrawViewGradientBottom:(NSString *)bottomColor Top:(NSString *)topColor andSelfDotView:(UIImageView *)view{
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    UIColor *bottomC;
    if(bottomColor==nil)
        bottomC= [UIColor clearColor];
    else bottomC=[self colorWithHexString:bottomColor];
    //    UIColor *middleC= [self colorWithHexString:midColor];
    UIColor *topC= [self colorWithHexString:topColor];
    //(0.0, 0.0) is the top left corner while (1.0, 1.0) is the bottom right corner
    theViewGradient.startPoint = CGPointMake(0.5, 0.0);
    theViewGradient.endPoint = CGPointMake(0.5, 1.0);
    theViewGradient.colors = [NSArray arrayWithObjects: (id)bottomC.CGColor,(id)topC.CGColor, nil];
    theViewGradient.frame = CGRectMake(0, 0, view.frame.size.width, view.frame.size.height);
    
    //Add gradient to view
    [view.layer insertSublayer:theViewGradient atIndex:0];
}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+(UIImage *)convertImageToGrayScale:(UIImage *)image {
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, image.size.width, image.size.height);
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, image.size.width, image.size.height, 8, 0, colorSpace, kCGImageAlphaNone);
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    // Return the new grayscale image
    return newImage;
}

//----------------------------------------
#pragma mark - BOXES
//----------------------------------------
NSString * const BOX_id = @"BOX_id";
NSString * const BOX_type= @"BOX_type";

NSString * const BOX_titles = @"BOX_titles";
NSString * const BOX_imageURL = @"BOX_imageURL";
NSString * const BOX_titleOpacity = @"BOX_titleOpacity";
NSString * const BOX_images = @"BOX_images";
NSString * const BOX_imageWidth = @"BOX_imageWidth";
NSString * const BOX_imageHeight = @"BOX_imageHeight";
NSString * const BOX_yPOS = @"BOX_yPOS";
NSString * const BOX_scrollViewContentSize= @"BOX_scrollViewContentSize";


+ (void)makeRectangleBoxes:(NSDictionary *)options withScrollView:(UIScrollView *)myScrollView andViewController:(UIViewController *)vc{
    NSArray *myTitles = options[BOX_titles];
    CGFloat Titleopacity = [options[BOX_titleOpacity] floatValue];
    //----For UIImages in scrollView
    CGFloat imageWidth = vc.view.frame.size.width - 16;
    CGFloat imageHeight = vc.view.frame.size.height*1/4;
    CGFloat yPOS = [options[BOX_yPOS] floatValue];
    CGFloat scrollViewContentSize = [options[BOX_scrollViewContentSize]floatValue];
    
    //-Checking if someImageURLs have been sent and if BOX_id and type are being sent
    NSArray *myURLimages;
    NSArray *myImages ;
    BOOL flag_URL = NO;
    BOOL flag_exception = YES;
    for (NSString *key in options) {
        if ([key isEqualToString:BOX_imageURL]){
            flag_URL = YES;
            myURLimages = options[BOX_imageURL];
        } else if ([key isEqualToString:BOX_images]){
            myImages = options[BOX_images];
        } else if([key isEqualToString:BOX_type]){
            flag_exception = NO;
        } else if([key isEqualToString:BOX_imageHeight]){
            imageHeight = [options[BOX_imageHeight]doubleValue];
        }
    }
    
    //-Exceptions
    if(flag_exception==YES)
        [NSException raise:@"Utilities.m/Rectangle" format:@"BOX_type not in parameter list", flag_URL];
    else if([myTitles count]>[myURLimages count] && [myTitles count]>[myImages count])
        [NSException raise:@"Utilities.m/Rectangle" format:@"URLs sent=%d and titlecount > imagecount", flag_URL];
    
    NSArray *array_id = options[BOX_id];
    NSString *type = options[BOX_type];
    
    for(int i=0; i<[myTitles count]; i++){
        UIImageView *hotTile = [[UIImageView alloc] init];
        hotTile.frame = CGRectMake(8, yPOS, imageWidth, imageHeight);
        hotTile.layer.cornerRadius = 10;
        hotTile.contentMode = UIViewContentModeCenter;
        hotTile.contentMode = UIViewContentModeScaleAspectFill;
        hotTile.contentMode = UIViewContentModeCenter;
        hotTile.contentMode = UIViewContentModeScaleAspectFill;
        if(flag_URL) {
            [hotTile sd_setImageWithURL:[NSURL URLWithString:[myURLimages objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"back_no_trill"] options:SDWebImageRefreshCached];
            hotTile.clipsToBounds = YES;
        }
        else {
            UIImage *img = [UIImage imageNamed:[myImages objectAtIndex:i]];
            hotTile.image = img;
            hotTile.clipsToBounds = YES;
        }
        hotTile.layer.opacity = 0.8;
        //Shadow
        UIImageView *shadowView = [[UIImageView alloc] initWithFrame:hotTile.frame];
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
        shadowView.layer.masksToBounds = NO;
        shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
        shadowView.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
        shadowView.layer.shadowOpacity = 0.8f;
        shadowView.layer.shadowPath = shadowPath.CGPath;
        shadowView.layer.shadowRadius = hotTile.layer.cornerRadius;
        [myScrollView addSubview:shadowView];
        
        [myScrollView addSubview:hotTile];
        
        //-----Text title
        UILabel *title= [[UILabel alloc] init];
        title.frame = hotTile.frame;
        title.center = hotTile.center;
        title.text = [myTitles objectAtIndex:i];
        title.font = [UIFont fontWithName:@"Bebas" size:30];
        title.textColor = [UIColor whiteColor];
        title.layer.opacity = Titleopacity;
        
        title.textAlignment = NSTextAlignmentCenter;
        title.numberOfLines = 0;
        title.minimumScaleFactor = 0.3;
        title.adjustsFontSizeToFitWidth = YES;
        title.lineBreakMode = NSLineBreakByTruncatingTail;
        [myScrollView addSubview:title];
        
        //--aDDING Button for link
        UICustomButton *button = [[UICustomButton alloc] initWithFrame:hotTile.frame uniqueID:[array_id objectAtIndex:i] andType:type];
        button.vc = vc;
        [button addTarget:self action:@selector(buttonMethod:) forControlEvents:UIControlEventTouchUpInside];
        [myScrollView addSubview:button];
        
        yPOS+=imageHeight+10;
        scrollViewContentSize+=imageHeight+10;
        myScrollView.contentSize = CGSizeMake(imageWidth, scrollViewContentSize);
    }//end for
}

//-------Square
//-------------
+ (void)makeSquareBoxes:(NSDictionary *)options withScrollView:(UIScrollView *)myScrollView andViewController:(UIViewController *)vc{
    NSArray *myTitles = options[BOX_titles];
    CGFloat Titleopacity = [options[BOX_titleOpacity] floatValue];
    //----For UIImages in scrollView
    CGFloat imageWidth = vc.view.frame.size.width/2 - 8 - 4;
    CGFloat imageHeight = imageWidth;
    CGFloat yPOS = [options[BOX_yPOS] floatValue];
    CGFloat scrollViewContentSize = [options[BOX_scrollViewContentSize]floatValue];
    
    //-Checking if someImageURLs have been sent and if BOX_id and type are being sent
    NSArray *myURLimages;
    NSArray *myImages ;
    BOOL flag_URL = NO;
    BOOL flag_exception = YES;
    for (NSString *key in options) {
        if ([key isEqualToString:BOX_imageURL]){
            flag_URL = YES;
            myURLimages = options[BOX_imageURL];
        } else if ([key isEqualToString:BOX_images]){
            myImages = options[BOX_images];
        } else if([key isEqualToString:BOX_type]){
            flag_exception = NO;
        }
    }
    
    //-Exceptions
    if(flag_exception==YES)
        [NSException raise:@"Utilities.m/Square" format:@"BOX_type not in parameter list", flag_URL];
    else if([myTitles count]>[myURLimages count] && [myTitles count]>[myImages count])
        [NSException raise:@"Utilities.m/Square" format:@"URLs sent=%d and titlecount > imagecount", flag_URL];
    
    NSArray *array_id = options[BOX_id];
    NSString *type = options[BOX_type];
    
    for(int i=0; i<[myTitles count]; i+=2){
        for(int j=0; j<2 && (i+j<[myTitles count]); j++){
            UIImageView *hotTile = [[UIImageView alloc] init];
            hotTile.frame = CGRectMake(8 + j*(imageWidth+8), yPOS, imageWidth, imageHeight);
            hotTile.backgroundColor = [UIColor whiteColor];
            if(flag_URL) {
                [hotTile sd_setImageWithURL:[NSURL URLWithString:[myURLimages objectAtIndex:i+j]] placeholderImage:[UIImage imageNamed:@"back_no_trill"] options:SDWebImageRefreshCached];
                hotTile.contentMode = UIViewContentModeCenter;
                if(![options[BOX_type] isEqualToString:NULLVOID])               //if not NULLVOID.
                    hotTile.contentMode = UIViewContentModeScaleAspectFill;
                else
                    hotTile.contentMode = UIViewContentModeScaleAspectFit;      //Note : When in StoreView.m , it is NULLVOI
                hotTile.clipsToBounds = YES;
                //Shadow
                UIImageView *shadowView = [[UIImageView alloc] initWithFrame:hotTile.frame];
                UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
                shadowView.layer.masksToBounds = NO;
                shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
                shadowView.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
                shadowView.layer.shadowOpacity = 0.8f;
                shadowView.layer.shadowPath = shadowPath.CGPath;
                shadowView.layer.shadowRadius = 1.0;
                [myScrollView addSubview:shadowView];
            }
            else {
                UIImage *img = [UIImage imageNamed:[myImages objectAtIndex:i+j]];
                hotTile.image = img;
                hotTile.clipsToBounds = NO;
                //Shadow
                hotTile.layer.shadowColor = [UIColor blackColor].CGColor;
                hotTile.layer.shadowOffset = CGSizeMake(1, 2);
                hotTile.layer.shadowOpacity = 0.7;
                hotTile.layer.shadowRadius = 1.0;
            }
            hotTile.layer.opacity = 0.8;
            
            [myScrollView addSubview:hotTile];  //---addSubView HotTile
            //-----Text title
            UILabel *title= [[UILabel alloc] init];
            title.frame = hotTile.frame;
            title.center = hotTile.center;
            title.text = [myTitles objectAtIndex:i+j];
            title.font = [UIFont fontWithName:@"Bebas" size:30];
            title.textColor = [UIColor whiteColor];
            title.layer.opacity = Titleopacity;
            
            title.textAlignment = NSTextAlignmentCenter;
            title.numberOfLines = 0;
            title.minimumScaleFactor = 0.3;
            title.adjustsFontSizeToFitWidth = YES;
            title.lineBreakMode = NSLineBreakByTruncatingTail;
            [myScrollView addSubview:title];
            
            //--aDDING Button for link
            UICustomButton *button = [[UICustomButton alloc] initWithFrame:hotTile.frame uniqueID:[array_id objectAtIndex:i+j] andType:type];
            button.vc = vc;
            [button addTarget:self action:@selector(buttonMethod:) forControlEvents:UIControlEventTouchUpInside];
            [myScrollView addSubview:button];
            
        }//end j loop
        
        yPOS+=imageHeight+8;
        scrollViewContentSize+=imageHeight+8;
        myScrollView.contentSize = CGSizeMake(imageWidth, scrollViewContentSize);
    }//end for
}


+ (void)makehorizontalBoxes:(NSDictionary *)options withScrollView:(UIScrollView *)myScrollView andViewController:(UIViewController *)vc{
    NSArray *myTitles = options[BOX_titles];
    CGFloat Titleopacity = [options[BOX_titleOpacity] floatValue];
    //----For UIImages in scrollView
    CGFloat imageWidth = vc.view.frame.size.width - 64;
    CGFloat imageHeight = vc.view.frame.size.height*0.4;
    CGFloat yPOS = [options[BOX_yPOS] floatValue];
    CGFloat scrollViewContentSize = [options[BOX_scrollViewContentSize]floatValue];
    
    //-Checking if someImageURLs have been sent and if BOX_id and type are being sent
    NSArray *myURLimages;
    NSArray *myImages ;
    BOOL flag_URL = NO;
    BOOL flag_exception = YES;
    for (NSString *key in options) {
        if ([key isEqualToString:BOX_imageURL]){
            flag_URL = YES;
            myURLimages = options[BOX_imageURL];
        } else if ([key isEqualToString:BOX_images]){
            myImages = options[BOX_images];
        } else if([key isEqualToString:BOX_type]){
            flag_exception = NO;
        }else if([key isEqualToString:BOX_imageHeight]){
            imageHeight = [options[BOX_imageHeight]doubleValue];
        }
    }
    
    //-Exceptions
    if(flag_exception==YES)
        [NSException raise:@"Utilities.m/horizontalBoxes" format:@"BOX_type not in parameter list", flag_URL];
    else if([myTitles count]>[myURLimages count] && [myTitles count]>[myImages count])
        [NSException raise:@"Utilities.m/horizontalBoxes" format:@"URLs sent=%d and titlecount > imagecount", flag_URL];
    
    NSArray *array_id = options[BOX_id];
    NSString *type = options[BOX_type];
    CGFloat xPOS = 0 ;
    UIScrollView *horizontalScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, yPOS, vc.view.frame.size.width, imageHeight+5)];
    horizontalScrollView.showsHorizontalScrollIndicator = NO;
    
    for(int i=0; i<[myTitles count]; i++){
        UIImageView *hotTile = [[UIImageView alloc] init];
        hotTile.frame = CGRectMake(24+xPOS, 0, imageWidth, imageHeight);
        
        if(flag_URL) {
            [hotTile sd_setImageWithURL:[NSURL URLWithString:[myURLimages objectAtIndex:i]] placeholderImage:[UIImage imageNamed:@"back_no_trill"] options:SDWebImageRefreshCached];
            hotTile.contentMode = UIViewContentModeCenter;
            hotTile.contentMode = UIViewContentModeScaleAspectFill;
            hotTile.clipsToBounds = YES;
            //Shadow
            UIImageView *shadowView = [[UIImageView alloc] initWithFrame:hotTile.frame];
            UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
            shadowView.layer.masksToBounds = NO;
            shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
            shadowView.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
            shadowView.layer.shadowOpacity = 0.8f;
            shadowView.layer.shadowPath = shadowPath.CGPath;
            shadowView.layer.shadowRadius = 1.0;
            [horizontalScrollView addSubview:shadowView];
        }
        else {
            UIImage *img = [UIImage imageNamed:[myImages objectAtIndex:i]];
            hotTile.image = img;
            hotTile.clipsToBounds = NO;
            //Shadow
            hotTile.layer.shadowColor = [UIColor blackColor].CGColor;
            hotTile.layer.shadowOffset = CGSizeMake(1, 2);
            hotTile.layer.shadowOpacity = 0.7;
            hotTile.layer.shadowRadius = 1.0;
        }
        hotTile.layer.opacity = 0.9;
        [Utilities colorDrawViewGradientBottom:nil Top:@"000000" andSelfDotView:hotTile];
        [horizontalScrollView addSubview:hotTile];
        
        //-----Text title
        UILabel *title= [[UILabel alloc] init];
        title.frame = CGRectMake(10, hotTile.frame.size.height - 40, hotTile.frame.size.height-10, 30);
        title.text = [myTitles objectAtIndex:i];
        title.font = [UIFont fontWithName:@"Bebas" size:30];
        title.textColor = [UIColor whiteColor];
        title.layer.opacity = Titleopacity;
        
        title.textAlignment = NSTextAlignmentLeft;
        title.numberOfLines = 0;
        title.minimumScaleFactor = 0.3;
        title.adjustsFontSizeToFitWidth = YES;
        title.lineBreakMode = NSLineBreakByTruncatingTail;
        [hotTile addSubview:title];
        
        //--aDDING Button for link
        UICustomButton *button = [[UICustomButton alloc] initWithFrame:hotTile.frame uniqueID:[array_id objectAtIndex:i] andType:type];
        button.vc = vc;
        [button addTarget:self action:@selector(buttonMethod:) forControlEvents:UIControlEventTouchUpInside];
        [horizontalScrollView addSubview:button];
        
        horizontalScrollView.contentSize = CGSizeMake(scrollViewContentSize, yPOS);
        xPOS+=imageWidth+8;
        scrollViewContentSize+=imageWidth+8;
    }//end for
    [myScrollView addSubview:horizontalScrollView];
}

//-----------------------------------------------
#pragma mark - Box BUTTON click Actions
//-----------------------------------------------
+(void)buttonMethod:(UICustomButton *)button{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
#pragma mark Offer CONTROLLER
    if(button.type == OFFER){
        
        if([button.unique_id isEqualToString:@"1"])
            [button.vc performSegueWithIdentifier:@"CouponViewSegue" sender:nil];
        
        else if([button.unique_id isEqualToString:@"2"]){
            NSString *condition = @"WHERE is_seen=1 ORDER BY name ASC";
            NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
            if([tableData count]==0){
                [Utilities showAlertWithTitle:@"No coupons" andMessage:@"You have not unlocked any coupon. Go to our nearest mall to recieve more coupons."];
                return;
            }
            [button.vc performSegueWithIdentifier:@"UnlockedRecentlyViewSegue" sender:nil];
        }
        else if([button.unique_id isEqualToString:@"3"]){   //Favorites
            NSString *condition = @"WHERE is_fav=1 ORDER BY name ASC";
            NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
            if([tableData count]==0){
                [Utilities showAlertWithTitle:@"No coupons" andMessage:@"You have not marked any coupon as your favourite."];
                return;
            }
            
            UIViewController *vc = [[UIViewController alloc]init];
            CGFloat leaveTopHeight = button.vc.navigationController.navigationBar.frame.size.height + 30;
            //---ScrollView
            UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, vc.view.frame.size.width, vc.view.frame.size.height)];
            [vc.view addSubview:myScrollView];
            
            vc.view.backgroundColor = [Utilities colorWithHexString:black_one];
            
            //----For UIImages in scrollView
            NSArray *myTitles = @[@"1",@"2", @"3", @"4"];
            CGFloat Titleopacity = 1.0f;
            NSArray *myImages = @[@"back_no_trill",@"back_no_trill",@"back_no_trill",@"back_no_trill"];
            CGFloat imageWidth = button.vc.view.frame.size.width - 32;
            CGFloat imageHeight = button.vc.view.frame.size.height*1/4;
            CGFloat yPOS = 16 ;
            CGFloat scrollViewContentSize = yPOS + leaveTopHeight + 100;//+ 50 for height of the TabMenuBar
            
            //---Retrieval from Database
            NSMutableArray *unique_id = [[NSMutableArray alloc] init];
            NSMutableArray *titles = [[NSMutableArray alloc] init];
            NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
            for(int i=0; i<[tableData count]; i++){
                NSString *key = [NSString stringWithFormat:@"%d",i];
                NSDictionary *dict = [tableData valueForKey:key];
                [unique_id addObject:[dict valueForKey:@"id"]];
                [titles addObject:[dict valueForKey:@"brand_name"]];
                [image_URLs addObject:[dict valueForKey:@"image"]];
            }
            NSDictionary *parameters = @{BOX_type: COUPON,
                                         BOX_id: unique_id,
                                         
                                         BOX_titles: titles,
                                         BOX_imageURL: image_URLs,
                                         BOX_titleOpacity: @(Titleopacity),
                                         BOX_images: myImages,
                                         BOX_imageWidth: @(imageWidth),
                                         BOX_imageHeight: @(imageHeight),
                                         BOX_yPOS: @(yPOS),
                                         BOX_scrollViewContentSize:@(scrollViewContentSize)
                                         };
            [Utilities makeSquareBoxes:parameters withScrollView:myScrollView andViewController:vc];
            button.vc.navigationController.navigationBar.barTintColor = [Utilities colorWithHexString:black_one];
            button.vc.navigationController.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
            
            [button.vc.navigationController pushViewController:vc animated:YES];
        }//@end else if 3
    }
#pragma mark Utility CONTROLLER
    else if(button.type == UTILITY){
        NSString *regionID = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
        if([regionID isEqualToString:@"8cd0d099-94b3-4b6e-2040-5e86cd442092"]){
            [Utilities showAlertWithTitle:@"Not Available" andMessage:@"Please select a mall"];
            return;
        }
        
        if([button.unique_id isEqualToString:@"1"]){
            NSString *condition = [NSString stringWithFormat:@"WHERE zone_id='%@' ORDER BY F_Level ASC",regionID];
            NSDictionary *tableData = [[appDelegate DB]displayTable:@"store_data" preCondition:@"SELECT DISTINCT F_Level,store_location" selectfromTableNamePLUSCondition:condition];
            if ([tableData count]==0){
                [Utilities showAlertWithTitle:@"Stores N/A" andMessage:@"Store data for this mall is being updated"];
                return;
            } else
                [button.vc performSegueWithIdentifier:@"StoreViewSegue" sender:nil];
        }
        else if([button.unique_id isEqualToString:@"2"]){
            NSString *condition = [NSString stringWithFormat:@"WHERE zone_id='%@' ORDER BY F_Level ASC",regionID];
            NSDictionary *tableData = [[appDelegate DB]displayTable:@"zone_map" preCondition:nil selectfromTableNamePLUSCondition:condition];
            if([tableData count]==0){
                [Utilities showAlertWithTitle:@"MallMap" andMessage:@"Maps cannot be fetched for this mall currently"];
                return;
            }
            [button.vc performSegueWithIdentifier:@"MallMapSegue" sender:nil];
        }
    }
#pragma mark Coupon
    else if(button.type == COUPON) {
        //----Fetching Details for coupon
        [self couponCardwithID:button.unique_id andViewController:button.vc];
    }
#pragma mark Category
    else if(button.type == CATEGORY) {
        //----Fetching Details for coupon
//        NSString *condition = [NSString stringWithFormat:@"WHERE category='%@'",button.unique_id];
        NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:nil];
        NSMutableArray *unique_id = [[NSMutableArray alloc] init];
        NSMutableArray *titles = [[NSMutableArray alloc] init];
        NSMutableArray *image_URLs = [[NSMutableArray alloc] init];
        for(int i=0; i<[tableData count]; i++){
            NSString *key = [NSString stringWithFormat:@"%d",i];
            NSDictionary *dict = [tableData valueForKey:key];
           
            NSString *category_dict = [dict valueForKey:@"category"];
            NSLog(@"\nCategory1: %@", category_dict);
            category_dict = [category_dict stringByReplacingOccurrencesOfString:@"(" withString:@""];
            category_dict = [category_dict stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            category_dict = [category_dict stringByReplacingOccurrencesOfString:@"\"" withString:@""];
            category_dict = [category_dict stringByReplacingOccurrencesOfString:@" " withString:@""];
            category_dict = [category_dict stringByReplacingOccurrencesOfString:@")" withString:@""];
            NSLog(@"\nCategory2: %@", category_dict);
            
            NSMutableArray *separated_array = [category_dict componentsSeparatedByString:@","];
            NSLog(@"\narray: %@", separated_array);
            
            for(NSString *str in separated_array)
                if([button.unique_id isEqualToString:str]) {
                    [unique_id addObject:[dict valueForKey:@"id"]];
                    [titles addObject:[dict valueForKey:@"brand_name"]];
                    [image_URLs addObject:[dict valueForKey:@"image"]];
                }
        }
        NSDictionary *parameters = @{   BOX_id: unique_id,
                                        BOX_type: COUPON,
                                        BOX_titles: titles,
                                        BOX_imageURL: image_URLs,
                                        BOX_titleOpacity: @(1.0),
                                        BOX_yPOS: @(25),
                                        BOX_scrollViewContentSize:@(75)
                                };
        if([unique_id count]==0) {
            [Utilities showAlertWithTitle:@"Category" andMessage:@"No coupons in this category currently"];
            return;
        }
        UIViewController *vc_temp = [[UIViewController alloc] init];//--viewcontroller
        vc_temp.view.backgroundColor = [Utilities colorWithHexString:black_one];
        UIScrollView *temp_categoryScrollView = [[UIScrollView alloc] initWithFrame:vc_temp.view.frame];//ScrollView
        [self makeSquareBoxes:parameters withScrollView:temp_categoryScrollView andViewController:vc_temp];
        [vc_temp.view addSubview:temp_categoryScrollView]; //---ScrollView addition
        //--navigation Bar props
        button.vc.navigationController.navigationBar.barTintColor = [Utilities colorWithHexString:black_one];
        button.vc.navigationController.navigationBar.tintColor = [Utilities colorWithHexString:secondary];
        CGFloat leaveTopHeight = 0;//button.vc.navigationController.navigationBar.frame.size.height + 30;
        UIView *solidcolor = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, vc_temp.view.frame.size.width, leaveTopHeight)];
        solidcolor.backgroundColor = [Utilities colorWithHexString:black_one];
        [vc_temp.view addSubview:solidcolor];
        
        [button.vc.navigationController pushViewController:vc_temp animated:YES];
    }//@end for [tabledata count]
    return;
}

//----------------
#pragma mark - CardPopOver
//----------------
+ (void)couponCardwithID:(NSString*)unique_identifier andViewController:(UIViewController *)current_vc{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *condition = [NSString stringWithFormat:@"WHERE id='%@'",unique_identifier];
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
    current_vc = current_vc.tabBarController;   //--------------------viewcontroller to tabbarcontroller
    [self cardPopOpen:tableData withButton:NO andViewController:current_vc];
}

+ (void)cardPopOpen:(NSDictionary *)tableData withButton:(BOOL)flag_button andViewController:(UIViewController *)current_vc{
    if([tableData count]==0) //If Nothing Found Return
        return;
    //----Fetching Details for coupon
    NSString *unique_id;
    NSString *brand;
    NSString *name;
    NSString *descp;
    NSString *image_URL;
    int is_fav=0;
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        unique_id = [dict valueForKey:@"id"];
        brand = [dict valueForKey:@"brand_name"];
        name = [dict valueForKey:@"name"];
        descp = [dict valueForKey:@"descp"];
        image_URL = [dict valueForKey:@"image"];
        is_fav = [[dict valueForKey:@"is_fav"]integerValue];
    }
   
    //---Making View
    UIView *popOverCoupon = [[UIView alloc] initWithFrame:current_vc.view.frame];
    UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
    opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
    opaqueImage.layer.opacity = 0.75;
    [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
    //Button to ClosePopOver touch outside bounds
    UICustomButton *close_popover =[[UICustomButton alloc] initCloseButtonWithFrame:popOverCoupon.frame andView:popOverCoupon];
    close_popover.vc = current_vc;
    [close_popover addTarget:self action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_popover]; //----Add close BUTTON on popOverView
    
    CGFloat imageSize = popOverCoupon.frame.size.height*5/(7*3);
    //---CardView
    UIImageView *card = [[UIImageView alloc] initWithFrame:CGRectMake(20, popOverCoupon.frame.size.height/7, popOverCoupon.frame.size.width-40, imageSize+50)];
    card.backgroundColor = [Utilities colorWithHexString:@"ffffff"];
    //-Image on card
    UIImageView *card_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, card.frame.size.width, imageSize)];
    [card_image sd_setImageWithURL:[NSURL URLWithString:image_URL] placeholderImage:[UIImage imageNamed:@"back_no_trill"]];
    card_image.contentMode = UIViewContentModeCenter;
    card_image.contentMode = UIViewContentModeScaleAspectFill;
    card_image.clipsToBounds = YES;
    [card addSubview:card_image];
    //-Text TITLE
    CGFloat fontSize = 30;
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(8, card_image.frame.size.height + 8, card_image.frame.size.width - 16, fontSize+2)];
    title.text = brand;
    title.font = [UIFont fontWithName:@"Raleway-Bold" size:fontSize];
    title.textColor = [Utilities colorWithHexString:bar_primary];
    //        title.contentMode = UIViewContentModeCenter;
    title.minimumScaleFactor = 0.5;
    title.adjustsFontSizeToFitWidth = YES;
    [card addSubview:title];
    
    //***********Description Dynamic SIZE
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(title.frame.origin.x, title.frame.origin.y + fontSize, title.frame.size.width, 30.0f)];
    // add text to the UITextView first so we know what size to make it
    textView.text = descp;
    textView.font = [UIFont fontWithName:@"Raleway-Medium" size:16.5];
    textView.textColor = [Utilities colorWithHexString:primary];
    textView.backgroundColor = [UIColor clearColor];
    // get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    [card addSubview:textView];
    //-Changing card frame
    card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, card.frame.size.height + newFrame.size.height +50);
    card.center = popOverCoupon.center;
    //-Shadow for card
    UIImageView *shadowView = [[UIImageView alloc] initWithFrame:card.frame];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
    shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(2.0f, 3.0f);
    shadowView.layer.shadowOpacity = 0.9f;
    shadowView.layer.shadowPath = shadowPath.CGPath;
    shadowView.layer.shadowRadius = 1.0;
    [popOverCoupon addSubview:shadowView];//--Add shadow on popOverView
    //***********
    
    [popOverCoupon addSubview:card]; //--Add card on popOverView
    
    //Resizing card if BUTTON is enabled
    CGFloat elevation=50;
    if(flag_button==YES){
        elevation = 100;
        card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, card.frame.size.height+elevation/2);
        card.center = popOverCoupon.center;
    }
    
    //---Making cross button for closing
    UIImageView *close = [[UIImageView alloc] initWithFrame:CGRectMake( card.frame.size.width + card.frame.origin.x -40, card.frame.origin.y, 40, 40)];
    close.layer.shadowColor = [[UIColor blackColor]CGColor];
    close.layer.masksToBounds = NO;
    close.layer.shadowColor = [UIColor blackColor].CGColor;
    close.layer.shadowOffset = CGSizeMake(1.2f, 1.2f);
    close.layer.shadowOpacity = 0.35f;
    close.layer.shadowRadius = 1.0;
    //close.backgroundColor = [Utilities colorWithHexString:@"1b9fc6"];
    close.layer.opacity = 0.95f;
    close.image = [UIImage imageNamed:@"cross_white"];
    [popOverCoupon addSubview:close]; //--Add close VIEW on popOverView
    //-Adding CLOSE button
    UICustomButton *close_button =[[UICustomButton alloc]initCloseButtonWithFrame:close.frame andView:popOverCoupon];
    close_button.vc = current_vc;
    //        close_button.backgroundColor = [UIColor redColor];
    [close_button addTarget:self action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_button]; //----Add close BUTTON on popOverView
    
    //---Making FAVORITE button
    CGRect fav_frame = CGRectMake( card.frame.origin.x, card.frame.origin.y+card.frame.size.height - elevation , 50, 50);
    UIImageView *favorite = [[UIImageView alloc] initWithFrame:fav_frame];
    favorite.layer.shadowColor = [[UIColor blackColor]CGColor];
    favorite.layer.masksToBounds = NO;
    favorite.layer.shadowColor = [UIColor blackColor].CGColor;
    favorite.layer.shadowOffset = CGSizeMake(1.5f, 3.0f);
    favorite.layer.shadowOpacity = 0.95f;
    favorite.layer.shadowRadius = 1.0;
    favorite.layer.opacity = 1.0f;
    //-Add INTERACTION
    UICustomButton *tapGesture1 = [[UICustomButton alloc] initFavButtonWithFrame:fav_frame uniqueID:unique_id  andImageView:favorite];
    [tapGesture1 addTarget:self action:@selector(favButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    if(is_fav == 0){
        tapGesture1.imageView.image = [UIImage imageNamed:@"ic_fav_off"];
        tapGesture1.tag = 0;
    }
    else{
        tapGesture1.imageView.image = [UIImage imageNamed:@"ic_fav_on"];
        tapGesture1.tag = [NSNumber numberWithInt:1];
    }
    
    [popOverCoupon addSubview:tapGesture1.imageView]; //----Add fav icon VIEW on popOverView
    [popOverCoupon addSubview:tapGesture1];  //----Add fav BUTTON on popOverView
    //END
    
    if(flag_button==YES){
        CGFloat btn_height = elevation - 50;
        //----Adding UIButton ON pOPoVERView
        CGRect button_frame = CGRectMake( card.frame.origin.x, card.frame.origin.y+card.frame.size.height-btn_height , card.frame.size.width,btn_height);
        UICustomButton *button_send = [[UICustomButton alloc] initCloseButtonWithFrame:button_frame andView:popOverCoupon];
        UIView *buttonView = [[UIView alloc] initWithFrame:button_send.frame];
        UILabel *label = [[UILabel alloc] initWithFrame:buttonView.frame];
        [buttonView addSubview:label];
        button_send.titleLabel.text = @"BUTTON";
        button_send.titleLabel.font = [UIFont fontWithName:@"Raleway-Regular" size:16];
        button_send.titleLabel.textColor = [Utilities colorWithHexString:@"1b9fc6"];
        //-Adding target of button
        [button_send addTarget:self action:@selector(ButtonUnlockRecent:) forControlEvents:UIControlEventTouchUpInside];
        SWRevealViewController *SW = current_vc;
        UITabBarController *TB = SW.frontViewController;
        UINavigationController *NC = TB.selectedViewController;
        UIViewController *VC = NC.viewControllers[0];
        button_send.vc = VC;
        //
        [Utilities colorDrawViewGradientLeft:secondary Right:bar_secondary andSelfDotView:buttonView];
        [popOverCoupon addSubview:buttonView];
        UILabel *button_heading = [[UILabel alloc]initWithFrame:buttonView.frame];
        button_heading.text = @"SHOW RECENT OFFERS";
        button_heading.textAlignment = NSTextAlignmentCenter;
        button_heading.font = [UIFont fontWithName:@"Raleway-Bold" size:16];
        button_heading.textColor = [UIColor whiteColor];
        [popOverCoupon addSubview:button_heading];
        [popOverCoupon addSubview:button_send];
    }
    
    [popOverCoupon setUserInteractionEnabled:YES];
    [card setUserInteractionEnabled:YES]; //The touch events always come to the view on top, unless cardView touch is enabled.
    [textView setUserInteractionEnabled:NO]; //To disable editing of textView
    [current_vc.view addSubview:popOverCoupon];
}


+ (void)cardZone:(NSDictionary *)tableData andViewController:(UIViewController *)current_vc{
    if([tableData count]==0) //If Nothing Found Return
        return;
    //----Fetching Details for coupon
    NSString *unique_id;
    NSString *brand;
    NSString *name;
    NSString *descp;
    NSString *image_URL;
    int is_fav=0;
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        unique_id = [dict valueForKey:@"zone_id"];
        brand = [dict valueForKey:@"zone_name"];
        descp = [dict valueForKey:@"descp"];
        image_URL = [dict valueForKey:@"zone_img"];
    }
    
    //---Making View
    UIView *popOverCoupon = [[UIView alloc] initWithFrame:current_vc.view.frame];
    UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
    opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
    opaqueImage.layer.opacity = 0.7;
    [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
    UICustomButton *close_popover =[[UICustomButton alloc] initCloseButtonWithFrame:popOverCoupon.frame andView:popOverCoupon];
    close_popover.vc = current_vc;
    [close_popover addTarget:[Utilities class] action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_popover]; //----Add close BUTTON on popOverView
    
    //---CardView
    UIImageView *card = [[UIImageView alloc] initWithFrame:CGRectMake(20, popOverCoupon.frame.size.height/7, popOverCoupon.frame.size.width-40, popOverCoupon.frame.size.height*5/7)];
    card.backgroundColor = [Utilities colorWithHexString:black_one];
    card.layer.opacity = 0.92;
    card.layer.cornerRadius = 10;
    card.clipsToBounds = YES;
    //-Shadow for card
    UIImageView *shadowView = [[UIImageView alloc] initWithFrame:card.frame];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
    shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowColor = [[Utilities colorWithHexString:secondary]CGColor];
    shadowView.layer.shadowOffset = CGSizeMake(2.0f, 3.0f);
    shadowView.layer.shadowOpacity = 0.2f;
    shadowView.layer.shadowPath = shadowPath.CGPath;
    shadowView.layer.shadowRadius = card.layer.cornerRadius/2;
    [popOverCoupon addSubview:shadowView];//--Add shadow on popOverView
    //-Image on card
    UIImageView *card_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, card.frame.size.width, card.frame.size.height/3*0.85)];
    [Utilities colorDrawViewGradientLeft:secondary Right:bar_secondary andSelfDotView:card_image];
    //[card_image sd_setImageWithURL:[NSURL URLWithString:image_URL] placeholderImage:[UIImage imageNamed:@"back_no_trill"]];
    //card_image.contentMode = UIViewContentModeCenter;
    //card_image.contentMode = UIViewContentModeScaleAspectFill;
    card_image.clipsToBounds = YES;
    [card addSubview:card_image];
    //-Text TITLE
    CGFloat titleHeight = 30;
    UILabel *title= [[UILabel alloc] initWithFrame:CGRectMake(16, (card_image.frame.size.height-titleHeight)/2-15, card_image.frame.size.width - 24, titleHeight)];
    title.text = brand;
    title.font = [UIFont fontWithName:@"Raleway-Bold" size:30];
    title.textColor = [Utilities colorWithHexString:@"ffffff"];
    title.numberOfLines = 0;
    title.lineBreakMode =NSLineBreakByTruncatingTail;
    [title sizeToFit];
    title.contentMode = UIViewContentModeTop;
    //*****get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth0 = title.frame.size.width;
    CGSize newSize0 = [title sizeThatFits:CGSizeMake(fixedWidth0, MAXFLOAT)];
    CGRect newFrame0 = title.frame;
    newFrame0.size = CGSizeMake(fmaxf(newSize0.width, fixedWidth0), newSize0.height);
    title.frame = newFrame0;
    [card addSubview:title];    //----addSubview
    //--Description DESCP
    UILabel *subtitle= [[UILabel alloc] initWithFrame:CGRectMake(title.frame.origin.x, card_image.frame.origin.y+card_image.frame.size.height+16 , card_image.frame.size.width - 24,  20)];
    NSString *username = [[NSUserDefaults standardUserDefaults]objectForKey:user_name];
    subtitle.text = [NSString stringWithFormat:@"Hi %@,\nWelcome to %@.\n", username,brand,descp];
    subtitle.font = [UIFont fontWithName:@"Raleway-SemiBold" size:17];
    subtitle.textColor = [Utilities colorWithHexString:@"dfdfdf"];
    subtitle.numberOfLines = 0;
    subtitle.minimumScaleFactor = 0.5;
    subtitle.adjustsFontSizeToFitWidth = YES;
    subtitle.lineBreakMode = NSLineBreakByTruncatingTail;
    //subtitle.backgroundColor = [UIColor redColor];
    [subtitle sizeToFit];
    subtitle.contentMode = UIViewContentModeTop;
    //*****get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth = subtitle.frame.size.width;
    CGSize newSize = [subtitle sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = subtitle.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    subtitle.frame = newFrame;
    [card addSubview:subtitle]; //----addSubview
    //---AnimationZZZ
    NSString *myFilePath =  [[NSBundle mainBundle] pathForResource:@"gift_new" ofType:@"json"];
    NSData *myData = [NSData dataWithContentsOfFile:myFilePath];
    NSError *error = nil;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:myData options:kNilOptions error:&error];
    LOTAnimationView *aview = [LOTAnimationView animationFromJSON:jsonDict];
    CGFloat sizeOfAnim = card_image.frame.size.height*1.2;
    CGFloat difference = card.frame.size.width/3;
    CGFloat originOfAnimation = (subtitle.frame.origin.y + subtitle.frame.size.height-sizeOfAnim/4);
    aview.frame = CGRectMake( +sizeOfAnim/2, originOfAnimation+sizeOfAnim/7, sizeOfAnim , sizeOfAnim);
    aview.contentMode = UIViewContentModeScaleAspectFit;
    aview.loopAnimation = YES;
    [aview play];
    [card addSubview:aview];
    
    //---------Subheading2
    UILabel *subtitle2= [[UILabel alloc] initWithFrame:CGRectMake(subtitle.frame.origin.x, originOfAnimation+aview.frame.size.height,  card_image.frame.size.width - 24,  50)];
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Keep your app open and get Trill Points to unlock exclusive offers by sound as you walk."];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-SemiBold" size:17] range:NSMakeRange(18, string.length-18)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Medium" size:17] range:NSMakeRange(0, 18)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Medium" size:17] range:NSMakeRange(69, 6)];   //for 'sound' , unlock is at 42th
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:secondary] range:NSMakeRange(0,18)];
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:@"dfdfdf"] range:NSMakeRange(18, string.length-18)];
    [string addAttribute:NSForegroundColorAttributeName value:[Utilities colorWithHexString:tertiary] range:NSMakeRange(69,6)];    //for 'sound'
    subtitle2.attributedText = string;
    subtitle2.numberOfLines = 0;
    subtitle2.minimumScaleFactor = 0.5;
    subtitle2.adjustsFontSizeToFitWidth = YES;
    subtitle2.lineBreakMode = NSLineBreakByTruncatingTail;
    //subtitle2 = [UIColor redColor];
    [subtitle2 sizeToFit];
    subtitle2.contentMode = UIViewContentModeTop;
    //*****get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth2 = subtitle2.frame.size.width;
    CGSize newSize2 = [subtitle2 sizeThatFits:CGSizeMake(fixedWidth2, MAXFLOAT)];
    CGRect newFrame2 = subtitle2.frame;
    newFrame2.size = CGSizeMake(fmaxf(newSize2.width, fixedWidth2), newSize2.height);
    subtitle2.frame = newFrame2;
    [card addSubview:subtitle2]; //----addSubview
    
    card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, (subtitle2.frame.origin.y+subtitle2.frame.size.height+40));
    card.center = popOverCoupon.center;
    [popOverCoupon addSubview:card]; //--Add card on popOverView
    
    //---Making cross button for closing
    UIImageView *close = [[UIImageView alloc] initWithFrame:CGRectMake( card.frame.size.width + card.frame.origin.x -30.5, card.frame.origin.y+0.5, 30, 30)];
    close.layer.shadowColor = [[UIColor blackColor]CGColor];
    close.layer.masksToBounds = NO;
    close.layer.shadowColor = [UIColor blackColor].CGColor;
    close.layer.shadowOffset = CGSizeMake(1.2f, 1.2f);
    close.layer.shadowOpacity = 0.25f;
    close.layer.shadowRadius = 1.0;
    //close.backgroundColor = [Utilities colorWithHexString:@"1b9fc6"];
    close.layer.opacity = 0.75f;
    close.image = [UIImage imageNamed:@"cross_white"];
    [popOverCoupon addSubview:close]; //--Add close VIEW on popOverView
    //-Adding CLOSE button
    UICustomButton *close_button =[[UICustomButton alloc] initCloseButtonWithFrame:close.frame andView:popOverCoupon];
    close_button.frame = CGRectMake(close.frame.origin.x, close.frame.origin.x, 40, 40);
    close_button.center = close.center;
    close_button.vc = current_vc;
    
    [close_button addTarget:self action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_button]; //----Add close BUTTON on popOverView
    //END
    
    [popOverCoupon setUserInteractionEnabled:YES];
    [current_vc.view addSubview:popOverCoupon];
}

//-----------------------------------------------
#pragma mark - Check Campaign/Contest
//-----------------------------------------------

+(BOOL)checkContest:(NSDictionary *)tableData withView:(UIViewController *)current_vc {
    //current_vc = current_vc.tabBarController;
    if([tableData count]==0)
        return NO;
    
    //----Fetching Details for coupon
    NSString *unique_id;
    NSString *brand;
    NSString *name;
    NSString *descp;
    NSString *type;
    int is_used=0;
    for(int i=0; i<1; i++) {
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        unique_id = [dict valueForKey:@"id"];
        brand = [dict valueForKey:@"brand_name"];
        name = [dict valueForKey:@"name"];
        descp = [dict valueForKey:@"descp"];
        type = [dict valueForKey:@"type"];
        is_used = [[dict valueForKey:@"is_used"]integerValue];
    }
    
    if( ![@"CONTEST" caseInsensitiveCompare:type] == NSOrderedSame ) {
        return NO;
    }
    else if(is_used==1)
        return YES; //tells that it was a contest coupon only
        
    //---Making View
    UIView *popOverCoupon = [[UIView alloc] initWithFrame:current_vc.view.frame];
    UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
    opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
    opaqueImage.layer.opacity = 0.75;
    [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
    UICustomButton *close_popover =[[UICustomButton alloc] initCloseButtonWithFrame:popOverCoupon.frame andView:popOverCoupon];
    close_popover.vc = current_vc;
    [close_popover addTarget:[Utilities class] action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_popover]; //----Add close BUTTON on popOverView
    
    //---CardView
    CGFloat imageSize = popOverCoupon.frame.size.height*5/(7*3);
    UIImageView *card = [[UIImageView alloc] initWithFrame:CGRectMake(20, popOverCoupon.frame.size.height/7, popOverCoupon.frame.size.width-40, imageSize+70)];
    card.backgroundColor = [Utilities colorWithHexString:@"ffffff"];
    //-Image on card
    UIImageView *card_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, card.frame.size.width, imageSize)];
    card_image.image = [UIImage imageNamed:@"payTM"];
    card_image.contentMode = UIViewContentModeCenter;
    card_image.contentMode = UIViewContentModeScaleAspectFit;
    card_image.backgroundColor = [UIColor whiteColor];  //incase aspectFit leaves spaces, the spaces will be white coloured
    card_image.clipsToBounds = YES;
    [card addSubview:card_image];       //---addSubview on card
    
    //*********** Dynamic SIZE
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(8, card_image.frame.size.height + 8, card_image.frame.size.width - 16, 30.0f)];
    // add text to the UITextView first so we know what size to make it
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"PayTM cash giveaway,\nYou won\nRs.12\n\nENTER & REDEEM"];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Bold" size:18] range:NSMakeRange(0, 19)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Regular" size:18] range:NSMakeRange(19, 9)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Black" size:36] range:NSMakeRange(28, 7)];
    [string addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Raleway-Bold" size:16] range:NSMakeRange(35, 15)];
    textView.attributedText = string;
    textView.textAlignment = NSTextAlignmentCenter;
    textView.textColor = [Utilities colorWithHexString:primary];
    textView.backgroundColor = [UIColor clearColor];
    // get the size of the UITextView based on what it would be with the text
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
    [card addSubview:textView];     //--addSubview on card
    //********************
    //----Search textField
    CGFloat leaveGap =card_image.frame.size.height+8 + newFrame.size.height+8;
    textField = [[UITextField alloc] initWithFrame:CGRectMake(40, leaveGap, card_image.frame.size.width-80, 30 )];
    textField.textAlignment = NSTextAlignmentCenter;
    textField.keyboardType = UIKeyboardTypeNumberPad;
    textField.delegate = self;
    textField.placeholder = @"Enter Phone Number";
    textField.textColor = [Utilities colorWithHexString:primary];
    textField.returnKeyType = UIReturnKeyDefault;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.5f;
    border.borderColor = [Utilities colorWithHexString:@"b5b5b5"].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border]; //---addSubLayer Line
    textField.layer.masksToBounds = YES;
    //addSubView TextField done after tapGesture
    
    //---Changing card frame
    CGFloat newCardHeight = card.frame.size.height + (newFrame.size.height+textField.frame.size.height) + 20;
    card.frame = CGRectMake(card.frame.origin.x, card.frame.origin.y, card.frame.size.width, newCardHeight);
    card.center = popOverCoupon.center;
    //-Shadow for card
    UIImageView *shadowView = [[UIImageView alloc] initWithFrame:card.frame];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:shadowView.bounds];
    shadowView.layer.masksToBounds = NO;
    shadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(2.0f, 3.0f);
    shadowView.layer.shadowOpacity = 0.9f;
    shadowView.layer.shadowPath = shadowPath.CGPath;
    shadowView.layer.shadowRadius = 1.0;
    [popOverCoupon addSubview:shadowView];//--Add shadow on popOverView
    //
    [popOverCoupon addSubview:card]; //--Add card on popOverView
    
    //^^^^^GESTURE to card, to end editing of keyboard
    UICustomButton *tapGesture = [[UICustomButton alloc] init];
    tapGesture.frame = card.frame;
    tapGesture.vc = current_vc;
    [tapGesture addTarget:[Utilities class] action:@selector(closeKeyboard:) forControlEvents:UIControlEventTouchUpInside];
    [card addSubview:tapGesture];   //---Add subview Tapgesture
    [card addSubview:textField]; //---addSubview SearchTextField
    
    //---Making cross button for closing
    UIImageView *close = [[UIImageView alloc] initWithFrame:CGRectMake( card.frame.size.width + card.frame.origin.x -40, card.frame.origin.y, 40, 40)];
    close.layer.shadowColor = [[UIColor blackColor]CGColor];
    close.layer.masksToBounds = NO;
    close.layer.shadowColor = [UIColor blackColor].CGColor;
    close.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    close.layer.shadowOpacity = 0.25f;
    close.layer.shadowRadius = 1.0;
    //close.backgroundColor = [Utilities colorWithHexString:@"1b9fc6"];
    close.layer.opacity = 0.75f;
    close.image = [UIImage imageNamed:@"cross_black"];
    [popOverCoupon addSubview:close]; //--Add close VIEW on popOverView
    //-Adding CLOSE button
    UICustomButton *close_button =[[UICustomButton alloc] initCloseButtonWithFrame:close.frame andView:popOverCoupon];
    close_button.vc = current_vc;
    [close_button addTarget:[Utilities class] action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_button]; //----Add close BUTTON on popOverView

    //-------------------------------OK BUTTON
    CGFloat elevation=100;
    CGFloat btn_height = elevation - 50;
    //----Adding UIButton ON pOPoVERView
    CGRect button_frame = CGRectMake( card.frame.origin.x, card.frame.origin.y+card.frame.size.height-btn_height , card.frame.size.width,btn_height);
    UICustomButton *button_send = [[UICustomButton alloc] initCloseButtonWithFrame:button_frame andView:popOverCoupon];
    [button_send addTarget:[Utilities class] action:@selector(payTMsendTOserver:) forControlEvents:UIControlEventTouchUpInside];
    button_send.vc = current_vc;
    button_send.unique_id = unique_id;
    button_send.type = textField.text;
    button_send.tag = [NSNumber numberWithInt:12];
    
    UIView *buttonView = [[UIView alloc] initWithFrame:button_send.frame];
    [Utilities colorDrawViewGradientLeft:secondary Right:bar_secondary andSelfDotView:buttonView];
    
    UILabel *button_heading = [[UILabel alloc]initWithFrame:buttonView.frame];
    button_heading.text = @"OK";
    button_heading.textAlignment = NSTextAlignmentCenter;
    button_heading.font = [UIFont fontWithName:@"Raleway-Bold" size:16];
    button_heading.textColor = [UIColor whiteColor];
    [popOverCoupon addSubview:buttonView];
    [popOverCoupon addSubview:button_heading];
    [popOverCoupon addSubview:button_send];
    //---------------------------------------
    
    [popOverCoupon setUserInteractionEnabled:YES];
    [card setUserInteractionEnabled:YES]; //The touch events always come to the view on top, unless cardView touch is enabled.
    [textView setUserInteractionEnabled:NO]; //To disable editing of textView
    [button_send setUserInteractionEnabled:YES];
    [current_vc.view addSubview:popOverCoupon];
    return YES;
}


+(void)checkCampaign:(UIViewController *)current_vc{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *region = [[NSUserDefaults standardUserDefaults] objectForKey:@"region"];
    NSString *condition = [NSString stringWithFormat:@"WHERE LOWER(type)=LOWER('campaign') AND zone_id='%@'",region];
    NSDictionary *tableData = [[appDelegate DB] displayTable:@"coupon_info" preCondition:nil selectfromTableNamePLUSCondition:condition];
    if([tableData count]==0)
        return;
    
    current_vc = current_vc.tabBarController;       //--------------------viewcontroller to tabbarcontroller
    [self cardPopOpen:tableData withButton:NO andViewController:current_vc];
}

+(void)addTrillPoints:(int)points{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    #pragma mark SharedPreferences
    //-----Set in SharedPreferences
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *usertoken = [preferences objectForKey:@"usertoken"];
    int curr_trill_points = [preferences integerForKey:trill_points];
    int totalPoints = curr_trill_points + points;
    [preferences setInteger:totalPoints forKey:trill_points];
    
    BOOL isInternetAvailable = NO;
    //--Check internet connection
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        isInternetAvailable = NO;
    else isInternetAvailable = YES;

    NSDictionary *tableData = [appDelegate.DB displayTable:@"sync_data" preCondition:nil selectfromTableNamePLUSCondition:@"WHERE action_type ='trill_points'"];
    if([tableData count]==0){
        //---For sync dictionary
        NSNumber *newPoints = [NSNumber numberWithInt:totalPoints];
        //----Setting SYNC dict
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date_today = [NSDate date];
        NSString *stringFromDate = [dateFormat stringFromDate:date_today];
        NSMutableDictionary *sync_dictionary=[[NSMutableDictionary alloc] init];
        [sync_dictionary setValue:usertoken forKey:@"coupon_id"];
        [sync_dictionary setValue:trill_points forKey:@"action_type"];
        [sync_dictionary setValue:newPoints forKey:@"action_value"];
        [sync_dictionary setValue:stringFromDate forKey:@"timestamp"];
        [appDelegate.DB insertInSyncTable:sync_dictionary];
    } else {
        [[appDelegate DB] updateColumnofTable:@"sync_data" withColumnName:@"action_value" unique_id:usertoken andValue:[NSString stringWithFormat:@"%d",totalPoints] isInteger:YES];
    }
    
    if(isInternetAvailable)
        [self syncOnInternet];
}

+(void)initilizeKicks:(int)kicks {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UIViewController *current_vc = [appDelegate window].rootViewController;
    //----popOver
    UIView *popOverCoupon = [[UIView alloc] initWithFrame:current_vc.view.frame];
    UICustomButton *close_popover =[[UICustomButton alloc] initCloseButtonWithFrame:popOverCoupon.frame andView:popOverCoupon];
    close_popover.vc = current_vc;
    UIImageView *opaqueImage = [[UIImageView alloc] initWithFrame:popOverCoupon.frame];
    opaqueImage.layer.backgroundColor = [[UIColor blackColor]CGColor];
    opaqueImage.layer.opacity = 0.5;
    [popOverCoupon addSubview:opaqueImage];//--Add blackCover on popOverView
    [close_popover addTarget:[Utilities class] action:@selector(closeButtonMethod:) forControlEvents:UIControlEventTouchUpInside];
    [popOverCoupon addSubview:close_popover]; //----Add close BUTTON on popOverView
    
    //-Image on popOver
    CGFloat sizeOfCircle = popOverCoupon.frame.size.height*1/5;
    UIImageView *card_image = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, sizeOfCircle, sizeOfCircle)];
    card_image.center = popOverCoupon.center;
    card_image.image = [UIImage imageNamed:@"tbLogo_empty"];
    card_image.contentMode = UIViewContentModeCenter;
    card_image.contentMode = UIViewContentModeScaleAspectFit;
    card_image.clipsToBounds = YES;
    [popOverCoupon addSubview:card_image];       //---addSubview on card
    
    //--Label Points
    UILabel *points = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, sizeOfCircle*0.75, sizeOfCircle*0.75)];
    points.center = card_image.center;
    points.text= [NSString stringWithFormat:@"%d",kicks];
    points.textColor = [UIColor whiteColor];
    points.font = [UIFont fontWithName:@"Bebas" size:35];
    points.textAlignment = NSTextAlignmentCenter;
    [popOverCoupon addSubview:points];       //---addSubview on card
    //--Label Points
    UILabel *trill_label = [[UILabel alloc]initWithFrame:CGRectMake(points.frame.origin.x, points.frame.origin.y+points.frame.size.height*0.68, points.frame.size.width, 20)];
    trill_label.text= @"POINTS";
    trill_label.textColor = [Utilities colorWithHexString:tertiary];
    trill_label.font = [UIFont fontWithName:@"Raleway-Black" size:9];
    trill_label.textAlignment = NSTextAlignmentCenter;
    [popOverCoupon addSubview:trill_label];       //---addSubview on card
    
    [current_vc.view addSubview:popOverCoupon];
    [UIView animateWithDuration:2.0 animations:^{
        card_image.transform = CGAffineTransformMakeScale(1.4, 1.4);
        } completion:^(BOOL finished){
            //NEST 1
            [UIView animateWithDuration:2.0 animations:^{
                card_image.transform = CGAffineTransformMakeScale(1, 1);
            } completion:^(BOOL finished){
                //NEST2
                [UIView animateWithDuration:2.0 animations:^{
                    popOverCoupon.alpha = 0;
                } completion:^(BOOL finished){
                    //NEST3
                    [self closeButtonMethod:close_popover];
                }];
            }];
    }];
}

//----------------
#pragma mark - Buttons on CARD
//----------------
+(void)payTMsendTOserver:(UICustomButton*)button{
    NSLog(@"Entered Utilities/payTMsendTOserver");
    button.type = textField.text;
    NSString *phone = button.type;
    
    NSCharacterSet *s = [NSCharacterSet characterSetWithCharactersInString:@"1234567890"];
    s = [s invertedSet];
    NSRange r = [phone rangeOfCharacterFromSet:s];
    if (r.location != NSNotFound )
        NSLog(@"the string contains illegal characters");
    else if([phone length]!=10){
        NSLog(@"Less than 10 characters");
        [Utilities showAlertWithTitle:@"Enter Phone Number" andMessage:@"The number entered must contain 10 digits"];
        return;
    }
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    BOOL isInternetAvailable = NO;
    //--Check internet connection
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        isInternetAvailable = NO;
    else
        isInternetAvailable = YES;
    
    if(isInternetAvailable==YES){
        NSString *usertoken = [[NSUserDefaults standardUserDefaults] objectForKey:@"usertoken"];
        NSString *region_id = [[NSUserDefaults standardUserDefaults] objectForKey:region_identity];
        
        NSString *url_string = [NSString stringWithFormat:@"%@\/contest\/",URLhit];
        NSURL *url = [NSURL URLWithString: url_string];
        NSDictionary *dict = [[NSMutableDictionary alloc]init];
        [dict setValue:button.unique_id forKey:@"coupon_id"];
        [dict setValue:phone forKey:@"mobile"];
        [dict setValue:button.tag forKey:@"amount"];
        [dict setValue:region_id forKey:@"zone_id"];
        
        NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:usertoken forHTTPHeaderField:@"usertoken"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:requestData];
        NSLog(@"\n\nPayTM button payload : %@ \n...", dict);
        [Utilities sendRequestToServer:request message:@"Utilities.m/ favButtonTap" withResponseCallback:^(NSDictionary *responseJSON) {
            [self actionUploadwithID:button.unique_id withActionType:@"is_used" andValue:[NSNumber numberWithInt:1]];
            [[appDelegate DB] updateColumnofTable:@"coupon_info" withColumnName:@"is_used" unique_id:button.unique_id andValue:@"1" isInteger:YES];
            NSLog(@"\n\nPayTM button tap response : %@ \n...", responseJSON);
        }];
    }//@end IF
    else {
        [Utilities showAlertWithTitle:@"Internet Unavailable" andMessage:@"To redeem this offer you need to be connected to the internet"];
        return;
    }
    //------
    
    //----Taken from CloseButtonMethod:
    [button.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [button.TOPview removeFromSuperview];
}


+(void)closeButtonMethod:(UICustomButton *)button{
    [button.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [button.TOPview removeFromSuperview];
}

+(void)ButtonUnlockRecent: (UICustomButton *) button {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"UnlockedRecentlyView"];
    UINavigationController *nc = button.vc.navigationController;
    [nc pushViewController:vc animated:YES];

    //----Taken from CloseButtonMethod:
    [button.TOPview.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
    [button.TOPview removeFromSuperview];
}

+ (void)favButtonTap: (UICustomButton *)sender {
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    BOOL isInternetAvailable = NO;
    //--Check internet connection
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        isInternetAvailable = NO;
    else
        isInternetAvailable = YES;
    
    //If is_fav is OFF
    if([sender.tag integerValue] == 0){
        sender.tag = [NSNumber numberWithInt:1];
        sender.imageView.image = [UIImage imageNamed:@"ic_fav_on"];
        [[appDelegate DB] updateColumnofTable:@"coupon_info" withColumnName:@"is_fav" unique_id:sender.unique_id andValue:@"1" isInteger:YES];
    }
    //If is_fav is ON
    else if([sender.tag integerValue] == 1){
        sender.tag = [NSNumber numberWithInt:0];
        sender.imageView.image = [UIImage imageNamed:@"ic_fav_off"];
        [[appDelegate DB] updateColumnofTable:@"coupon_info" withColumnName:@"is_fav" unique_id:sender.unique_id andValue:@"0" isInteger:YES];
    }
    
    if(isInternetAvailable==YES){
        [self actionUploadwithID:sender.unique_id withActionType:@"is_fav" andValue:sender.tag];
    }//@end IF
    else {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date_today = [NSDate date];
        NSString *stringFromDate = [dateFormat stringFromDate:date_today];
        NSMutableDictionary *sync_dictionary=[[NSMutableDictionary alloc] init];
        [sync_dictionary setValue:sender.unique_id forKey:@"coupon_id"];
        [sync_dictionary setValue:@"is_fav" forKey:@"action_type"];
        [sync_dictionary setValue:sender.tag forKey:@"action_value"];
        [sync_dictionary setValue:stringFromDate forKey:@"timestamp"];
        
        [appDelegate.DB insertInSyncTable:sync_dictionary];
    }
}

//-------------------------------------------------------
#pragma mark - searchField Delegates
//-------------------------------------------------------
//-----for PAYTM contest closing keyboard with TAPGESTURE
+(void)closeKeyboard: (UICustomButton *) button{
    [button.vc.view endEditing:YES];
}

//-----------------------------------------------
#pragma mark - Others
//-----------------------------------------------
+ (void) syncOnInternet{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSString *usertoken = [[NSUserDefaults standardUserDefaults] objectForKey:@"usertoken"];
    
    NSDictionary *sync_dictionary = [appDelegate.DB displayTable:@"sync_data" preCondition:nil selectfromTableNamePLUSCondition:nil];
    if([sync_dictionary count]>0){
        NSMutableArray *array = [[NSMutableArray alloc]init];
        for (NSString *key in sync_dictionary) {
            [array addObject:[sync_dictionary objectForKey:key]];
        }
        NSMutableDictionary *final_dict = [[NSMutableDictionary alloc]init];
        [final_dict setObject:array forKey:@"sync"];
        
        NSString *url_string = [NSString stringWithFormat:@"%@\/sync\/",URLhit];
        NSURL *url = [NSURL URLWithString: url_string];
        NSData *requestData = [NSJSONSerialization dataWithJSONObject:final_dict options:NSJSONWritingPrettyPrinted error:nil];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod:@"POST"];
        [request setValue:usertoken forHTTPHeaderField:@"usertoken"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPBody:requestData];
        [Utilities sendRequestToServer:request message:@"(SYNC)Utilities.m/syncOnInternet" withResponseCallback:^(NSDictionary *responseJSON) {
            NSLog(@"\n\nSync Response : %@ \n...", responseJSON);
            [appDelegate.DB deleteSyncTable];
        }];
    }//@end if sync_dict count
}

+ (void) actionUploadwithID:(NSString*)unique_id withActionType:(NSString*)action_type andValue:(NSNumber*)value {
    NSString *usertoken = [[NSUserDefaults standardUserDefaults] objectForKey:@"usertoken"];
    NSString *url_string = [NSString stringWithFormat:@"%@\/coupons\/",URLhit];
    NSURL *url = [NSURL URLWithString: url_string];
    NSDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:unique_id forKey:@"id"];
    [dict setValue:value forKey:action_type];
    NSData *requestData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"PUT"];
    [request setValue:usertoken forHTTPHeaderField:@"usertoken"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:requestData];
    NSLog(@"\n\n%@ button payload : %@ \n...", action_type, dict);
    [Utilities sendRequestToServer:request message:@"Utilities.m/ favButtonTap" withResponseCallback:^(NSDictionary *responseJSON) {
        NSLog(@"\n\n%@ button tap response : %@ \n...", action_type, responseJSON);
    }];
}


+ (void) sendRequestToServer: (NSMutableURLRequest *)request message:(NSString*)msg withResponseCallback:(void(^)(NSDictionary *responseJSON))complete{
    NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig delegate: (id)self delegateQueue:nil];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
                                            completionHandler:
                                  ^(NSData *_Nullable data, NSURLResponse *_Nullable response, NSError *_Nullable error) {
                                      if (error) {
                                          // do something with the erroE
                                          NSLog(@"\n\n--Utilities/sendRequestToServer ERROR received \n...%@",msg);
                                          return;
                                      }
                                      NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                      if (httpResponse.statusCode == 200 || httpResponse.statusCode == 201) {
                                          NSDictionary* Response = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                          complete(Response);
                                      } else {
                                          // failure: do something else on failure
                                          NSLog(@"\n\n--Utilities/sendRequestToServer HTTP ERROR \n...%@",msg);
                                          NSLog(@"httpResponse code: %@", [NSString stringWithFormat:@"%ld", (unsigned long)httpResponse.statusCode]);
                                          NSLog(@"httpResponse head: %@", httpResponse.allHeaderFields);
                                          return;
                                      }
                                  }];
    [task resume];
}

+(void) skipLoginController: (BOOL)animate{
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UINavigationController *navigationController = (UINavigationController*) [appdelegate window].rootViewController;
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    //    UITabBarController *tabController = [story instantiateViewControllerWithIdentifier:@"tabBarController"];
    UIViewController *vc = [story instantiateViewControllerWithIdentifier:@"SWReveal"];
    
    [appdelegate window].rootViewController = vc;
}

+ (NSString*) getRegionNameWithID:(NSString*)zone_identifier{
    AppDelegate *appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSDictionary *tableData = [[appdelegate DB] displayTable:@"zone_info" preCondition:@"SELECT zone_name" selectfromTableNamePLUSCondition:[NSString stringWithFormat:@"WHERE zone_id='%@'",zone_identifier]];
    NSString *regionName = nil;
    for(int i=0; i<[tableData count]; i++){
        NSString *key = [NSString stringWithFormat:@"%d",i];
        NSDictionary *dict = [tableData valueForKey:key];
        regionName = [dict valueForKey:@"zone_name"];
    }
    return regionName;
}

+ (void) setRegionSharedPreference:(NSString*)zone_identifier{
#pragma mark SharedPreferences region
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:zone_identifier forKey:region_identity];
    //--Notification
    NSDictionary *userInfo = [NSDictionary dictionaryWithObject:zone_identifier forKey:region_identity];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDefaultRegion" object:nil userInfo:userInfo];
}


@end
             

