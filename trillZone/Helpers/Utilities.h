//
//  Utilities.h
//  trillZone
//
//  Created by Shivansh on 1/10/18.
//  Copyright © 2018 TrillBit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CustomButton.h"
#import "SWRevealView.h"
#import "ReachOut.h"
#import <Lottie/Lottie.h>

@class UICustomButton; //Because of ERROR in the  +(void)favButtonTap method

@interface Utilities : NSObject

//---Notification
#pragma mark - Notification
+ (void) showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;
+ (void) postNotification:(NSString *)title withMessage:(NSString *)message;

//---Others
#pragma mark - Others
+ (void) syncOnInternet;
+ (void) sendRequestToServer: (NSMutableURLRequest *)request message:(NSString*)msg withResponseCallback:(void(^)(NSDictionary *responseJSON))complete;
+ (void) skipLoginController:(BOOL)animate;
+ (NSString*) getRegionNameWithID:(NSString*)zone_id;
+ (void) setRegionSharedPreference:(NSString*)zone_identifier;


//---UI/UX
#pragma mark - UI/UX
+ (UIColor*)colorWithHexString:(NSString*)hex;
+ (void)colorDrawViewGradientLeft:(NSString *)leftColor Right:(NSString *)rightColor andSelfDotView:(UIView *)view;
+ (CAShapeLayer *)drawLineWithColour:(NSString*)color andLineWidth:(CGFloat)thickness;    //---->Note: add with [viewcontroller.view.layer addSublayer: shape] or [myscrollview addSublayer: shape]
+ (UIImage *)imageWithColor:(UIColor *)color;
+(UIImage *)convertImageToGrayScale:(UIImage *)image;

//---For rectangle and square buttons with TITLES on them
#pragma mark - Boxes
extern NSString * const BOX_id;
extern NSString * const BOX_type; //Category or OFFER
extern NSString * const BOX_titles;
extern NSString * const BOX_imageURL;
extern NSString * const BOX_titleOpacity;
extern NSString * const BOX_images;
extern NSString * const BOX_imageWidth;
extern NSString * const BOX_imageHeight;
extern NSString * const BOX_yPOS;
extern NSString * const BOX_scrollViewContentSize;
+ (void)makeRectangleBoxes:(NSDictionary *)options withScrollView:(UIScrollView *)myScrollView andViewController:(UIViewController *)vc;
+ (void)makeSquareBoxes:(NSDictionary *)options withScrollView:(UIScrollView *)myScrollView andViewController:(UIViewController *)vc;
+ (void)makehorizontalBoxes:(NSDictionary *)options withScrollView:(UIScrollView *)myScrollView andViewController:(UIViewController *)vc;
+ (void)initilizeKicks:(int)kicks;

//---For CardPopOver
#pragma mark - CardPopOver
+ (void)couponCardwithID:(NSString*)unique_identifier andViewController:(UIViewController *)current_vc;
+ (void)cardPopOpen:(NSDictionary *)tableData withButton:(BOOL)flag_button andViewController:(UIViewController *)current_vc;
+ (void)favButtonTap: (UICustomButton *)sender;
+ (void)cardZone:(NSDictionary *)tableData andViewController:(UIViewController *)current_vc;

//---Check Campaign/Contest
extern UITextField *textField;  //For phone number input textField in PAYTM contest
+(BOOL)checkContest:(NSDictionary *)tableData withView:(UIViewController *)current_vc;
+(void)checkCampaign:(UIViewController *)current_vc;    //Used in Homecontroller.m, Activated when notification of regionChange hits
+(void)addTrillPoints:(int)points;

@end


